import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import dataV from '@jiaminghi/data-view'
import axios from 'axios'
import * as echarts from 'echarts';
import ElementUI from 'element-ui'
Vue.use(ElementUI);
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/common.css'
Vue.prototype.$echarts = echarts

Vue.prototype.$axios= axios
Vue.use(dataV)
Vue.config.productionTip = false
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
