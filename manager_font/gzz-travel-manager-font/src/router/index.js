import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/views/login'
Vue.use(VueRouter)
// 初始化前端路由，可能有的数据
const routes = [

    {path:'/',redirect:'login'},
    {path: '/login', name: 'login', component: login},
    {path: '/homeView', name: 'homeView', component: ()=>import('@/views/HomeView.vue')},
    {path:'/layout', name:'layout',component:() => import("@/layout/Layout.vue"),
    redirect:"/ManageHome",
    children:[
      {
        path:'/ManageHome',name:'ManageHome',component:()=>import("@/views/ManageHome.vue")
      },
      {
        path:'/SceneryInfo',name:'SceneryInfo',component:()=>import("@/views/SceneryInfo.vue")
      },
      {
        path:'/StrategyInfo',name:'StrategyInfo',component:()=>import("@/views/StrategyInfo.vue")
      },
      {
        path:'/RouterInfo',name:'RouterInfo',component:()=>import("@/views/RouterInfo.vue")
      },
      {
        path:'/ActivityInfo',name:'ActivityInfo',component:()=>import("@/views/ActivityInfo.vue")
      },
      {
        path:'/TicketInfo',name:'TicketInfo',component:()=>import("@/views/TicketInfo.vue")
      },
      {
        path:'/TicketOrder',name:'TicketOrder',component:()=>import("@/views/TicketOrder.vue")
      },
      {
        path:'/GoodsOrder',name:'GoodsOrder',component:()=>import("@/views/GoodsOrder.vue")
      },
      {
        path:'/FeedBack',name:'FeedBack',component:()=>import("@/views/FeedBack.vue")
      },
      {
        path:'/LostObject',name:'LostObject',component:()=>import("@/views/LostObject.vue")
      },
      {
        path:'/WenGoodsInfo',name:'WenGoodsInfo',component:()=>import("@/views/WenGoodsInfo.vue")
      },
      {
        path:'/FoodInfo',name:'FoodInfo',component:()=>import("@/views/FoodInfo.vue")
      },
      {
        path:'/UserInfo',name:'UserInfo',component:()=>import("@/views/UserInfo.vue")
      },
      {
        path:'/NoticeInfo',name:'NoticeInfo',component:()=>import("@/views/NoticeInfo.vue")
      },
      {
        path:'/onlineUserInfo',name:'onlineUserInfo',component:()=>import("@/views/onlineUserInfo.vue")
      }
    ]}
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  // 1.如果访问的是登录页面（无需权限），直接放行
  if (to.path === '/login') return next()
  const token= window.sessionStorage.getItem('token')
  if (!token) {
    alert("请登录")
    return next('/login')
  }
  next()
})

export default router
