import TMap from "@/utils/initmap";

export default (await import('vue')).defineComponent({
components: {
chinaMap,
counter,
R7DayNum,
spotNumVue,
agePieVue,
userFeedBackVue,
nowPlayerNumVue
},

data() {
return {
tMap: null,
total: 123456,
cur: 1234,
NowUserAxisdata: ["13:00", "13:03", "13:06", "13:09", "13:12", "13:15"],
NowUserData: [100, 123, 145, 167, 145, 156, 135],
useFeedData: [
{ value: 148, name: '不满意' },
{ value: 735, name: '一般' },
{ value: 580, name: '满意' },
],
R7dayData: [121, 222, 433, 344, 565, 466, 577],
AgeData: [
{ value: 448, name: '5-17' },
{ value: 735, name: '18-30' },
{ value: 580, name: '31-44' },
{ value: 284, name: '45-60' },
{ value: 120, name: '60以上' }
],
PlayerFromDataList: [
{
name: "黑龙江",
value: 44,
},
{
name: "湖南",
value: 23,
},
{
name: "云南",
value: 68,
},
{
name: "安徽",
value: 66,
},
{
name: "江西",
value: 33,
},
{
name: "吉林",
value: 43,
},
{
name: "湖北",
value: 143,
},
]
};
},
mounted() {
this.initAMap();
},
unmounted() {
this.tMap?.destroy();
},
methods: {
initAMap() {
TMap.init().then((TMap) => {
this.TXMap = TMap;
this.tMap = new TMap.Map("container", {
center: new TMap.LatLng(29.045664, 110.483699),
zoom: 16,
viewMode: "3D",
baseMap: {
type: 'satellite'
}
});
//移除控件缩放
this.tMap.removeControl(TMap.constants.DEFAULT_CONTROL_ID.ZOOM);

// 移除比例尺控件
this.tMap.removeControl(TMap.constants.DEFAULT_CONTROL_ID.SCALE);

// 移除旋转控件
this.tMap.removeControl(TMap.constants.DEFAULT_CONTROL_ID.ROTATION);

});
},
toManage() {
console.log(1111);
this.$router.push({ name: 'layout' });
}
},
created: {}
});
