import request from "@/utils/request";

export default {
    name: "Login",
    data() {
        return {
           
          
            form: {
                adminId: null,
                password: "",
            },
            rules: {
                adminId: [
                    {required: true, message: "请输入用户ID", trigger: "blur"},{type:'number',message:'必须是数字',trigger:'blur'}
                ],
                password: [{required: true, message: "请输入密码", trigger: "blur"}],
            },
        };
    },
    computed: {
        disabled() {
            const {adminId, password} = this.form;
            return Boolean(adminId && password);
        },
    },
    methods: {
        login() {
           const _this=this;
            
            this.$refs.form.validate((valid) => {
               console.log(111)
                if (valid) {
                    //登录请求
                    //url:"/admin/login",data:this.form
                    request.post("/admin/login",this.form).then(res=>{
                        if(res.code==='1'){
                            console.log(res.map)
							const tokendata = res.map;
							const token = tokendata["token"]
							console.log(token)
                            window.sessionStorage.setItem('token',token)
                          
                            _this.$router.push({ name: 'homeView' })
							
							
                        }else{
                            this.$message({
                                type:'error',
                                message:res.msg
                            })
                        }

                    }).catch(()=>{
                        this.$message({
                            type:'error',
                            message:'服务异常'
                        })
                    })
                   
                }
            });
        },
    },
};