import axios from 'axios'
import $router from '@/router'
import { Message } from 'element-ui'
// 负责人编写http请求工具类，封装axios发送http请求
const request = axios.create({
    // 8.138.28.104
    baseURL: 'http://8.138.28.104:7777/api',  // 注意！！ 这里是全局统一加上了前缀，也就是说所有接口都会加上前缀在
    timeout: 5000
})

// request 拦截器 http://8.138.28.104/
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
//添加一个请求拦截器
request.interceptors.request.use(function (config) {
    const token = sessionStorage.getItem("token")
    console.log(token)
    if(token){
        config.headers.token = token;
    }
    //console.dir(config);
    return config;
}, function (error) {
    // Do something with request error
    console.info("error: ");
    console.info(error);
    return Promise.reject(error);
});
// response 拦截器
// 可以在接口响应后统一处理结果
request.interceptors.response.use(
    response => {
        let res = response.data;
        // 如果是返回的文件
        if (response.config.responseType === 'blob') {
            return res
        }
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        return res;
    },
    error => {
        console.log('err' + error) // for debug
        if(error.message.includes("timeout")){
          message.error("请求超时")
        }else{
            switch(error.response.status){
                // 对得到的状态码的处理，具体的设置视自己的情况而定
                case 401:
                    Message.error("请登录")
                    $router.push({path:'/'})
                    break
                case 404:
                    Message.error("请求找不到了")
                    break
                case 405:
                    console.log('不支持的方法')
                    break
                // case ...
                default:
                   
                    console.log('其他错误')
                    break
              }
              
        }
        return Promise.reject(error)
        
        
    }
)

// 以request暴露出去
export default request

