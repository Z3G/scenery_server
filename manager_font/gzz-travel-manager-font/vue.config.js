const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath:'/',transpileDependencies:true,
  devServer: {
    historyApiFallback:true,
    client: {
      overlay: false,
    },
  },
  
  configureWebpack: {
    resolve: { fallback: { fs: false } },
    externals:{
      './cptable': 'var cptable'
},
  }

})
