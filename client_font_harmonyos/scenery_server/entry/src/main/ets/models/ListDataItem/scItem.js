export class scItemModel {
    constructor(model) {
        this.scId = model.scId;
        this.scImage = model.scImage;
        this.scName = model.scName;
        this.scDesc = model.scDesc;
        this.scPrice = model.scPrice;
        this.scTime = model.scTime;
        this.scImages = model.scImages;
        this.scDatail = model.scDatail;
        this.scAddress = model.scAddress;
        this.scPhone = model.scPhone;
    }
}
export const DefaultData = [{
        scId: "",
        scImage: "",
        scName: "",
        scDesc: "",
        scPrice: "",
        scTime: '',
        scImages: [],
        scDatail: "",
        scAddress: "",
        scPhone: '',
    }];
//# sourceMappingURL=scItem.js.map