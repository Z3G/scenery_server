export class ticketOrderModel {
    constructor(model) {
        this.orderId = model.orderId;
        this.sumPrice = model.sumPrice;
        this.num = model.num;
        this.createTime = model.createTime;
        this.status = model.status;
        this.scName = model.scName;
    }
}
// 初始空数据
export const DefaultTkData = [{
        orderId: '',
        status: 0,
        scName: '',
        createTime: '',
        num: 0,
        sumPrice: 0,
    }];
//# sourceMappingURL=ticketOrder.js.map