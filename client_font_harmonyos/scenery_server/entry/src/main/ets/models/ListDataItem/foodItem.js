export class foodItemModel {
    constructor(model) {
        this.distant = model.distant;
        this.foodAddress = model.foodAddress;
        this.foodTips = model.foodTips;
        this.visits = model.visits;
        this.score = model.score;
        this.foodCoverImg = model.foodCoverImg;
        this.foodId = model.foodId;
        this.foodName = model.foodName;
        this.foodTime = model.foodTime;
        this.imgs = model.imgs;
        this.foodPhone = model.foodPhone;
        this.foodContent = model.foodContent;
    }
}
export const defaultFoodData = [{
        distant: 0,
        foodAddress: ' ',
        foodTips: '',
        visits: 0,
        score: 0,
        foodName: '',
        foodCoverImg: '',
        foodId: '',
        foodTime: '',
        imgs: [],
        foodPhone: "",
        foodContent: ""
    }];
//# sourceMappingURL=foodItem.js.map