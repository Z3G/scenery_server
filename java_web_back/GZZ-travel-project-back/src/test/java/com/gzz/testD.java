package com.gzz;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.mapper.PlayTableMapper;
import com.gzz.pojo.PlayTable;
import com.gzz.pojo.Ticket;
import com.gzz.service.PlayTableService;
import com.gzz.service.TicketService;
import com.gzz.utils.IDCardsUitls;
import com.gzz.utils.TimeUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SpringBootTest(classes = GzzProjectApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class testD {
	@Autowired
	TicketService ticketService;
	@Autowired
	PlayTableService playTableService;
	@Resource
	PlayTableMapper playTableMapper;
	@Test
	public void testDate(){
		LambdaQueryWrapper<Ticket> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String d = format.format(date);
		lambdaQueryWrapper.lt(Ticket::getTkDate,d);
		List<Ticket> list = ticketService.list(lambdaQueryWrapper);
		System.out.println(list);
	}
	@Test
	public void setData(){
		//随机生成
		Set<Integer> set = IDCardsUitls.province.keySet();
		String[] Pro = new String[100];
		int i = 1;
		for(Integer integer : set){
			Pro[i++] = integer.toString();
		}
		System.out.println(i);
		String[] Address = new String[9000];
		int tt = 1;
		for(Integer j = 1010;j<=8999;j++){
			Address[tt++] = j.toString();
		}
		String[] BirthdayYear = new String[100];
		String[] BirthdayMonth = new String[20];
		String[] BirthdayDay = new String[32];
		i = 1;

		for(Integer t =1950;t<=2017;t++){
			BirthdayYear[i++] = t.toString();
		}
		i=1;
		for(Integer t=1;t<=12;t++){
			if(t<10){
				BirthdayMonth[i++] = "0"+t.toString();
			}else{
				BirthdayMonth[i++] = t.toString();
			}

		}
		i=1;
		for(Integer t=1;t<=27;t++){
			BirthdayDay[i++] = t.toString();
		}
		for(int jj = 1;jj<=200;jj++){
			String idCard = "";
			int f1 = Math.abs(UUID.randomUUID().hashCode())%32;
			if(f1==0){
				f1=1;
			}
			idCard +=Pro[f1];
			int f2 = Math.abs(UUID.randomUUID().hashCode())%(8999-1010);
			if(f2==0){
				f2=1;
			}
			idCard+=Address[f2];
			int f3 = Math.abs(UUID.randomUUID().hashCode())%(2017-1950);
			if(f3==0){
				f3=1;
			}
			idCard+=BirthdayYear[f3];
			int f4 = Math.abs(UUID.randomUUID().hashCode())%13;
			if(f4==0){
				f4=1;
			}
			idCard+=BirthdayMonth[f4];
			int f5 = Math.abs(UUID.randomUUID().hashCode())%28;
			if(f5==0){
				f5=1;
			}
			idCard+=BirthdayDay[f5];
			f2 = Math.abs(UUID.randomUUID().hashCode())%(8999-1010);
			if(f2==0){
				f2=1;
			}
			idCard+=Address[f2];

			Long scId = (long)Math.abs(UUID.randomUUID().hashCode())%8;
			Long PlayerId = (long)Math.abs(UUID.randomUUID().hashCode());
			Date date = TimeUtils.getRomdDateTime();

			int v1 = Math.abs(UUID.randomUUID().hashCode())%8;
			int v2 = Math.abs(UUID.randomUUID().hashCode())%8;
			PlayTable playTable = new PlayTable();
			playTable.setIdCard(idCard);
			if(v1>=3){
				playTable.setStartTime(date);
			}
			if(v1>=4&&v2>=3){
				LocalDateTime localDateTime = TimeUtils.DateToLocalDatetime(date);
				localDateTime = localDateTime.plusHours((long)Math.abs(UUID.randomUUID().hashCode())%4+2);
				localDateTime = localDateTime.plusMinutes((long)Math.abs(UUID.randomUUID().hashCode())%30);
				date = Date.from(localDateTime.atZone(ZoneId.of("Asia/Shanghai")).toInstant());
				playTable.setEndTime(date);
			}
			playTable.setScId(scId);
			playTable.setPalyerId(PlayerId);

			playTableMapper.insert(playTable);

		}

	}





}

