package com.gzz.mapper;

import com.gzz.pojo.TicketOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface TicketOrderMapper extends BaseMapper<TicketOrder> {
	@Select("select count(*) from ticket_order where user_id = #{userId}")
	Integer getCountByUserId(Long userId);
	@Select("SELECT SUM(sum_price) FROM ticket_order WHERE user_id = #{userId};")
	BigDecimal getSumPricebyUserId(Long userId);
}
