package com.gzz.mapper;

import com.gzz.pojo.Strategy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface StrategyMapper extends BaseMapper<Strategy> {
	@Update("update strategy set st_sc = st_sc+#{add} where st_id = #{id}")
	void updateScById(Long id,Integer add);
	@Select("select count(*) from strategy where user_id = #{userId}")
	Integer getCountbyUserId(Long userId);
}
