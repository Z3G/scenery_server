package com.gzz.mapper;

import com.gzz.pojo.UserAdvise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface UserAdviseMapper extends BaseMapper<UserAdvise> {

}
