package com.gzz.mapper;

import com.gzz.pojo.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
