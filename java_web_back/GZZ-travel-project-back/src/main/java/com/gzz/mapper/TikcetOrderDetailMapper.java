package com.gzz.mapper;

import com.gzz.pojo.TikcetOrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口，门票订单详情
 * </p>
 */
public interface TikcetOrderDetailMapper extends BaseMapper<TikcetOrderDetail> {

}
