package com.gzz.mapper;

import com.gzz.pojo.SystemMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface SystemMessageMapper extends BaseMapper<SystemMessage> {

}
