package com.gzz.mapper;

import com.gzz.pojo.Scenery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface SceneryMapper extends BaseMapper<Scenery> {
	/**
	 * fetch data by rule id
	 * @author zjc
	 * @create_time 2023-11-26
	 * @return List<Long>
	 */
	@Select("select sc_id from scenery")
	List<Long> getAllId();
	@Select("select sc_id from scenery where sc_price>0")
	List<Long> getAllChargeId();
	@Select("select * from scenery order by order_wight limit 4")
	List<Scenery> getTopFour();
}
