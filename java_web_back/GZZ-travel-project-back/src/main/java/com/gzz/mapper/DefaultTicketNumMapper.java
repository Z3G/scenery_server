package com.gzz.mapper;

import com.gzz.pojo.DefaultTicketNum;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface DefaultTicketNumMapper extends BaseMapper<DefaultTicketNum> {

}
