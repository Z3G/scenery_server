package com.gzz.mapper;

import com.gzz.pojo.Ticket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface TicketMapper extends BaseMapper<Ticket> {
	@Delete("delete from ticket where sc_id = #{scId}")
	void deletebyscId(Long scId);
}
