package com.gzz.mapper;

import com.gzz.pojo.ImgsResp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口 图片存储服务
 * </p>
 *
 */
public interface ImgsRespMapper extends BaseMapper<ImgsResp> {

}
