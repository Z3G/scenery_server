package com.gzz.mapper;

import com.gzz.pojo.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
