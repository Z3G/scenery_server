package com.gzz.mapper;

import com.gzz.pojo.PlayerPerson;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface PlayerPersonMapper extends BaseMapper<PlayerPerson> {

}
