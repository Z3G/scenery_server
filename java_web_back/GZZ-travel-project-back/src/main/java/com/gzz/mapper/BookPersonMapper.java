package com.gzz.mapper;

import com.gzz.pojo.BookPerson;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface BookPersonMapper extends BaseMapper<BookPerson> {

}
