package com.gzz.mapper;

import com.gzz.pojo.UserSc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 收藏记录表 Mapper 接口
 * </p>
 *
 */
public interface UserScMapper extends BaseMapper<UserSc> {
	@Update("update user_sc set status = #{status} where user_id = #{userId} and st_id = #{stId}")
	void updateStatusByUserIdAndstId(Long userId,Long stId,Integer status);
}
