package com.gzz.mapper;

import com.gzz.pojo.GoodsOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface GoodsOrderMapper extends BaseMapper<GoodsOrder> {

}
