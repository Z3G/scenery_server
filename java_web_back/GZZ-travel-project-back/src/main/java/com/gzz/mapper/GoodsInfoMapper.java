package com.gzz.mapper;

import com.gzz.pojo.GoodsInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface GoodsInfoMapper extends BaseMapper<GoodsInfo> {

}
