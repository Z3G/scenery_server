package com.gzz.mapper;

import com.gzz.pojo.Lost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface LostMapper extends BaseMapper<Lost> {

}
