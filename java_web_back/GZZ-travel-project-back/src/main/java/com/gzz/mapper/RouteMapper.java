package com.gzz.mapper;

import com.gzz.pojo.Route;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface RouteMapper extends BaseMapper<Route> {

}
