package com.gzz.mapper;

import com.gzz.pojo.Cart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface CartMapper extends BaseMapper<Cart> {

}
