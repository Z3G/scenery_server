package com.gzz.mapper;

import com.gzz.pojo.BaseInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface BaseInfoMapper extends BaseMapper<BaseInfo> {
	@Select("select count(*) from base_info where user_id = #{userId}")
	Integer getCountByUserId(Long userId);
	@Select("SELECT SUM(sum_Price) FROM base_info WHERE user_id = #{userId};")
	Integer getSumPricebyUserId(Long userId);
}
