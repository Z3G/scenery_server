package com.gzz.mapper;

import com.gzz.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzz.vo.UserDetailVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface UserMapper extends BaseMapper<User> {
	public List<UserDetailVo> getCount(String userName, Long userId, Integer status, String userArea, Integer sex);
}
