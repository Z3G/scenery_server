package com.gzz.mapper;

import com.gzz.pojo.UserLikes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 点赞记录表 Mapper 接口
 * </p>
 *
 */
public interface UserLikesMapper extends BaseMapper<UserLikes> {

}
