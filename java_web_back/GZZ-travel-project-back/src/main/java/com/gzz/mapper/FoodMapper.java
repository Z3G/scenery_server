package com.gzz.mapper;

import com.gzz.pojo.Food;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface FoodMapper extends BaseMapper<Food> {

}
