package com.gzz.mapper;

import com.gzz.pojo.Tickettype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口 门票类型操作
 * </p>
 *
 */
public interface TickettypeMapper extends BaseMapper<Tickettype> {
	@Update("update tickettype set sale = #{sale} where type_id=1 and sc_id = #{scId}")
	void updateAdultSaleByScId(Long scId, BigDecimal sale);
	@Update("update tickettype set sale = #{sale} where type_id=3 and sc_id = #{scId}")
	void updateStudentSaleByScId(Long scId, BigDecimal sale);
}
