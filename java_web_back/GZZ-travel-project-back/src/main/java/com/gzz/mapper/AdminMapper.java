package com.gzz.mapper;

import com.gzz.pojo.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
