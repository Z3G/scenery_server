package com.gzz.mapper;

import com.gzz.pojo.BasePosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface BasePositionMapper extends BaseMapper<BasePosition> {

}
