package com.gzz.mapper;

import com.gzz.pojo.Scenery;
import com.gzz.pojo.Strategy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface HomePageMapper {
    @Select("select * from strategy where pub=1 and status=1")
    List<Strategy> getStrategyList();
    @Select("select * from scenery")
    List<Scenery> getSceneryList();
}
