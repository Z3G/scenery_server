package com.gzz.mapper;

import com.gzz.pojo.PlayTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface PlayTableMapper extends BaseMapper<PlayTable> {
	@Select("select count(*) from play_table where end_time is null and start_time is not null")
	Integer getNumBy3min();
	@Select("select count(*) from play_table where start_time is not null and start_time between #{sTime} and #{eTime}")
	Integer getTodayNum(Date sTime,Date eTime);
}
