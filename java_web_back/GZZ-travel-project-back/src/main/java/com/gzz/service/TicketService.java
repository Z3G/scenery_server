package com.gzz.service;

import com.gzz.common.R;
import com.gzz.pojo.Scenery;
import com.gzz.pojo.Ticket;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.TicketListVo;

import javax.naming.InsufficientResourcesException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类 门票服务接口
 * </p>
 *
 * @author 周佳成
 * @since 2023-11-20
 */
public interface TicketService extends IService<Ticket> {

	List<Integer> getMapForDateAndNum(Long scId);
	public void UpdateTicketEveryday();

	R<List<TicketListVo>> getTicketAdminList(String search, Integer value, String sDate);
	Integer getCount(String search, Integer value, String sDate);

	void addByScenery(Scenery scenery);

	void removeByscId(Long scId);

	Ticket creatTicketByTkDateAndScId(Long scI, Date d);
}
