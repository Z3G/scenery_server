package com.gzz.service.impl;

import com.gzz.pojo.SystemMessage;
import com.gzz.mapper.SystemMessageMapper;
import com.gzz.service.SystemMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class SystemMessageServiceImpl extends ServiceImpl<SystemMessageMapper, SystemMessage> implements SystemMessageService {

}
