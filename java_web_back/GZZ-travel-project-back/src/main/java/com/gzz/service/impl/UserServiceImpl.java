package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.gzz.common.R;
import com.gzz.mapper.BaseInfoMapper;
import com.gzz.mapper.StrategyMapper;
import com.gzz.mapper.TicketOrderMapper;
import com.gzz.pojo.User;
import com.gzz.mapper.UserMapper;
import com.gzz.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.vo.UserDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
	@Resource
	StrategyMapper strategyMapper;
	@Resource
	TicketOrderMapper ticketOrderMapper;
	@Resource
	BaseInfoMapper baseInfoMapper;

	@Resource
	UserMapper userMapper;
	@Override
	public List<UserDetailVo> getUserDetailVoList(String userName, Long userId, Integer status, String userArea, Integer sex) {


		List<UserDetailVo> res = new ArrayList<>();
		res = userMapper.getCount(userName, userId, status, userArea, sex);
		res.forEach(item->{

			Long id = item.getUserId();
			item.setOrderNum(ticketOrderMapper.getCountByUserId(id)+baseInfoMapper.getCountByUserId(id));

			item.setStrategyNum(strategyMapper.getCountbyUserId(userId));

			BigDecimal sumPricebyUserId = ticketOrderMapper.getSumPricebyUserId(userId)!=null ?  ticketOrderMapper.getSumPricebyUserId(userId):new BigDecimal(0);

			Integer tPrice = Integer.valueOf(sumPricebyUserId.intValue());

			Integer gPrice = baseInfoMapper.getSumPricebyUserId(id)!=null ?  baseInfoMapper.getSumPricebyUserId(id):0;

			item.setSumPrice(tPrice+gPrice);

		});

		return res;
	}
}
