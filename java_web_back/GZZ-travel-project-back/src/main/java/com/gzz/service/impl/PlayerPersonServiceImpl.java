package com.gzz.service.impl;

import com.gzz.pojo.PlayerPerson;
import com.gzz.mapper.PlayerPersonMapper;
import com.gzz.service.PlayerPersonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类，游玩人
 * </p>
 *
 */
@Service
public class PlayerPersonServiceImpl extends ServiceImpl<PlayerPersonMapper, PlayerPerson> implements PlayerPersonService {

}
