package com.gzz.service;

import com.gzz.common.R;
import com.gzz.pojo.Scenery;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.SenceryDetailVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface SceneryService extends IService<Scenery> {

	R<SenceryDetailVo> getDetailbyId(Long scId);

	R<List<SenceryDetailVo>> getDetailAll(String search,Integer value);
	Integer getTotalByCondition(String search,Integer value);
}
