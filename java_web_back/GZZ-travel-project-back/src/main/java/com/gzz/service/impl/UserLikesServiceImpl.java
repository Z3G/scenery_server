package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.Dto.UserLikesDTO;
import com.gzz.pojo.Strategy;
import com.gzz.pojo.UserLikes;
import com.gzz.mapper.UserLikesMapper;
import com.gzz.service.RedisService;
import com.gzz.service.StrategyService;
import com.gzz.service.UserLikesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.utils.RedisCache;
import com.gzz.utils.RedisKeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 点赞记录表 服务实现类
 * </p>
 *
 */
@Service
public class UserLikesServiceImpl extends ServiceImpl<UserLikesMapper, UserLikes> implements UserLikesService {

	@Autowired
	RedisCache redisCache;
	@Autowired
	RedisService redisService;
	@Autowired
	StrategyService strategyService;

	@Override
	@Scheduled(fixedDelay = 120*1000)
	public void updateFromRedis() {

		List<UserLikesDTO> list = new ArrayList<>();
		//得到了 key为id+userid的map对应的列表
		list = redisService.getLikedDataFromRedis();
		if(list==null || list.size()==0){
			return;
		}
		//得到更新了点赞的id
		Set<String> InfoIds = redisService.getUpdatedStId();


		//处理key为id+userid的map对应的集合
		for(UserLikesDTO item:list){
			Long infoId = item.getInfoId();
			Long usrId = item.getLikeUserId();
			Integer status = item.getStatus();
			LambdaQueryWrapper<UserLikes> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(UserLikes::getStId,infoId).eq(UserLikes::getUserId,usrId);
			UserLikes userLikes = getOne(lambdaQueryWrapper);

			if(userLikes==null||!(userLikes.getStatus().equals(status))) {
				if (userLikes == null) {
					Long id = (long) Math.abs(UUID.randomUUID().hashCode());
					userLikes = new UserLikes();
					userLikes.setId(id);
					userLikes.setCreateTime(new Date());
				}
				userLikes.setUserId(usrId);
				userLikes.setUpdateTime(new Date());
				userLikes.setStatus(status);
				userLikes.setStId(infoId);
				saveOrUpdate(userLikes);
			}
			//删除缓存
			redisService.deleteLikedFromRedis(infoId,usrId);
		}
		redisCache.deleteObject(RedisKeyUtils.MAP_KEY_USER_INFO);

		//处理key:MAP_KEY_USER_LIKED_COUNT对应的map缓存
		Map<String,Integer> map = new HashMap<>();
		//key:stId value:nums
		map = redisCache.getCacheMap(RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT);
		if(map==null || map.size()==0){
			return;
		}
		for(String key:InfoIds){
			Integer t = map.get(key);
			LambdaQueryWrapper<Strategy> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Strategy::getStId,Long.valueOf(key));
			Strategy strategy = strategyService.getOne(lambdaQueryWrapper);
			if(t!=0){
				Integer z = strategy.getStZan();
				strategy.setStZan(z+t);
				boolean r = strategyService.saveOrUpdate(strategy);
				if(r){
					redisCache.delCacheMapValue(RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT,key);
				}
			}else{
				redisCache.delCacheMapValue(RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT,key);
			}

		}

	}
}
