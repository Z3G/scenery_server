package com.gzz.service;

import com.gzz.pojo.DefaultTicketNum;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类，门票数量配置服务类
 * </p>
 *
 */
public interface DefaultTicketNumService extends IService<DefaultTicketNum> {

}
