package com.gzz.service.impl;

import com.gzz.pojo.BasePosition;
import com.gzz.mapper.BasePositionMapper;
import com.gzz.service.BasePositionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class BasePositionServiceImpl extends ServiceImpl<BasePositionMapper, BasePosition> implements BasePositionService {

}
