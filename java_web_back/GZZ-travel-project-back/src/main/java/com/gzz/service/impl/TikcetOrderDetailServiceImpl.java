package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.Dto.TicketOrderDto;
import com.gzz.mapper.BookPersonMapper;
import com.gzz.mapper.PlayerPersonMapper;
import com.gzz.pojo.BookPerson;
import com.gzz.pojo.PlayerPerson;
import com.gzz.pojo.TicketOrder;
import com.gzz.pojo.TikcetOrderDetail;
import com.gzz.mapper.TikcetOrderDetailMapper;
import com.gzz.service.TikcetOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.vo.TicketOrderDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类，门票订单详情
 * </p>
 *
 */
@Service
public class TikcetOrderDetailServiceImpl extends ServiceImpl<TikcetOrderDetailMapper, TikcetOrderDetail> implements TikcetOrderDetailService {

	@Resource
	PlayerPersonMapper playerPersonMapper;

	@Resource
	BookPersonMapper bookPersonMapper;
	@Override
	public List<TikcetOrderDetail> getListByDto(TicketOrderDto ticketOrderDto, TicketOrder ticketOrder) {
		BookPerson bookPerson = ticketOrderDto.getBookPerson();

		Long bookId = bookPerson.getId();

		List<PlayerPerson> list = ticketOrderDto.getPlayerPerson();

		Long OrderId = ticketOrder.getOrderId();

		List<TikcetOrderDetail> resList = new ArrayList<>();

		BigDecimal pr = ticketOrderDto.getPrice();

		for(PlayerPerson playerPerson:list){
			TikcetOrderDetail tikcetOrderDetail1 = new TikcetOrderDetail();
			tikcetOrderDetail1.setOrderId(OrderId);
			tikcetOrderDetail1.setBookId(bookId);
			tikcetOrderDetail1.setPrice(pr);
			tikcetOrderDetail1.setCreateTime(new Date());
			tikcetOrderDetail1.setTypeId(ticketOrderDto.getTypeId());
			tikcetOrderDetail1.setPalyerId(playerPerson.getId());
			resList.add(tikcetOrderDetail1);
		}

		return  resList;
	}

	@Override
	public List<TicketOrderDetailVo> getAllDetailVo(Long orderId) {
		LambdaQueryWrapper<TikcetOrderDetail> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(TikcetOrderDetail::getOrderId,orderId);
		List<TikcetOrderDetail> list = list(lambdaQueryWrapper);
		List<TicketOrderDetailVo> res = new ArrayList<>();
		list.forEach(item->{
			TicketOrderDetailVo ticketOrderDetailVo = new TicketOrderDetailVo();
			BeanUtils.copyProperties(item,ticketOrderDetailVo);
			Long bookId = item.getBookId();
			Long playerId = item.getPalyerId();
			String bookName = bookPersonMapper.selectById(bookId).getName();

			ticketOrderDetailVo.setBookName(bookName);

			String playerName = playerPersonMapper.selectById(playerId).getName();
			ticketOrderDetailVo.setPalyerName(playerName);
			ticketOrderDetailVo.setPrice(item.getPrice().intValue());
			Integer typeId = item.getTypeId();

			if(typeId==1){
				ticketOrderDetailVo.setTypeName("成人票");
			}else if(typeId==2){
				ticketOrderDetailVo.setTypeName("儿童票");
			}else{
				ticketOrderDetailVo.setTypeName("学生票");
			}

			res.add(ticketOrderDetailVo);

		});
		return  res;
	}
}
