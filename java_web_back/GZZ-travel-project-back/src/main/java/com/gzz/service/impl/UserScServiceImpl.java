package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.pojo.Strategy;
import com.gzz.pojo.UserSc;
import com.gzz.mapper.UserScMapper;
import com.gzz.service.StrategyService;
import com.gzz.service.UserScService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 收藏记录表 服务实现类
 * </p>
 *
 */
@Service
public class UserScServiceImpl extends ServiceImpl<UserScMapper, UserSc> implements UserScService {

	@Autowired
	StrategyService strategyService;

	@Override
	public List<Strategy> listByUserId(Long id) {
		List<UserSc> ScList = new ArrayList<>();
		LambdaQueryWrapper<UserSc> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(UserSc::getUserId,id).eq(UserSc::getStatus,1);
		ScList = list(lambdaQueryWrapper);
		if(ScList==null){
			return null;
		}
		List<Strategy> list = new ArrayList<>();

		for(UserSc it:ScList){
			Strategy strategy = strategyService.getById(it.getStId());
			list.add(strategy);
		}
		return list;
	}
}
