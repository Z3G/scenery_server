package com.gzz.service.impl;

import cn.hutool.core.util.StrUtil;
import com.aliyuncs.kms.transform.v20160120.CreateAliasResponseUnmarshaller;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.gzz.common.R;
import com.gzz.pojo.Activity;
import com.gzz.mapper.ActivityMapper;
import com.gzz.pojo.ImgsResp;
import com.gzz.pojo.Strategy;
import com.gzz.service.ActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.service.ImgsRespService;
import com.gzz.utils.OssUtils;
import com.gzz.utils.TimeUtils;
import com.gzz.vo.ActivityVo;
import com.gzz.vo.SenceryDetailVo;
import com.gzz.vo.StrategyDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
// 使用mybatisplus框架，点开ServiceImpl类会发现它提供了一系列对数据进行增删改查的函数
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements ActivityService {
	@Autowired
	ImgsRespService imgsRespService;

	// ActivityVo 封装好的返回给前端进行展示的活动详情数据，根据id查询活动详情信息
	@Override
	public R<ActivityVo> getDetailbyId(Long actId) {
		// 通过id从数据库中获取活动信息，利用的是mybatisplus提供的方法
		Activity activity = getById(actId);
		ActivityVo activityVo = new ActivityVo();
		// 对象属性复制，springboot框架提供的
		BeanUtils.copyProperties(activity,activityVo);
		// 或者该活动对应的图片，new StrategyDetailVo()是占位的，
		// 从数据库中查这个活动对应的详情图片列表
		List<String> imgs = imgsRespService.getlistByIdAndTypeId(new StrategyDetailVo(), actId,
				OssUtils.ActivityTypeId);
		if(imgs == null){
			return R.error("数据异常");
		}
		// 设置活动信息中图片列表的属性
		activityVo.setActImages(imgs);

		return R.success(activityVo);
	}

	@Override
	public R<List<ActivityVo>> getDetailAll(String search,Integer value) {
		// 需要返回的活动结果列表，ActivityVo 是封装好的返回给前端进行展示的活动详情数据，而不是返回Activity
		List<ActivityVo> res = new ArrayList<>();
		// 使用的是mybatisplus提供的条件查询器
		LambdaQueryWrapper<Activity> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		// like模糊匹配，相当于 '活动名' like '%search%'，如果search不为空才会条件生效
		lambdaQueryWrapper.like(search!=null,Activity::getActName,search);
		// 调用mybatisplus提供的list方法，并且传入条件查询器，根据上诉条件进行查询
		List<Activity> list = list(lambdaQueryWrapper);
		// 遍历每个活动信息，计算活动的状态，已结束，未开始，进行中
		list.forEach(item->{
			// 获取活动的开启日期 如2021-11-12
			Date actDatetime = item.getActDatetime();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			// 日期格式转换
			String d = format.format(actDatetime);
			// 获取活动的开放开始时间 格式为 HH:MM-HH:MM (小时:分钟)
			// 如 08:00-17:00，分割成两个字符串，s[0]是开始时间08:00，s[1]是结束时间17:00
			String[] s = item.getActTime().split("-");
			// 判断当前活动状态，0表示未开始，1表示进行中，2表示已经结束
			// 传入的参数value： 0大于现在 1正在 2小于现在
			// 时间比较函数：1小于现在 -1大于现在 0等于现在的时间
			Integer v1 = TimeUtils.compare(d,s[0]);
			Integer v2 = TimeUtils.compare(d,s[1]);
			Integer comp = 0;
			// 当开始时间小于等于并且结束时间大于等于现在，则正在进行中
			if((v1==1||v1==0) && (v2==-1||v2==0)){
				comp = 1;
			}else if(v1==-1){ // 上面条件不符合时，当开始时间大于现在说明未开始
				comp = 0;
			}else if(v2==1){ // 否则就是已经结束
				comp = 2;
			}
			// 符合活动状态条件的则加入的结果集中，如果value为空说明，不进行这个条件的查询，那都符合条件
			if(comp.equals(value)||value==null){
				ActivityVo activityVo = getDetailbyId(item.getActId()).getData();
				res.add(activityVo);
			}

		});
		return R.success(res);

	}
}
