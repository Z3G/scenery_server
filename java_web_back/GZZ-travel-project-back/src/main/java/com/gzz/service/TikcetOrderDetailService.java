package com.gzz.service;

import com.gzz.Dto.TicketOrderDto;
import com.gzz.pojo.TicketOrder;
import com.gzz.pojo.TikcetOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.TicketOrderDetailVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  服务类，门票订单详情
 * </p>
 */
public interface TikcetOrderDetailService extends IService<TikcetOrderDetail> {

	List<TikcetOrderDetail> getListByDto(TicketOrderDto ticketOrderDto, TicketOrder ticketOrder);

	List<TicketOrderDetailVo> getAllDetailVo(Long orderId);
}
