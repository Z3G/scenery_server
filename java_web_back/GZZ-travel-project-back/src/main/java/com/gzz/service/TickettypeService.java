package com.gzz.service;

import com.gzz.pojo.Tickettype;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类，门票类型接口
 * </p>
 *
 */
public interface TickettypeService extends IService<Tickettype> {

}
