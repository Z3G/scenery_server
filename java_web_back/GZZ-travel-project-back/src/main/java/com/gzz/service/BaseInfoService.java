package com.gzz.service;

import com.gzz.common.R;
import com.gzz.pojo.BaseInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface BaseInfoService extends IService<BaseInfo> {

	R<List<BaseInfo>> getBaseInfoListbyAdmin(String search, Long userId, Integer status);
}
