package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.mapper.HomePageMapper;
import com.gzz.mapper.SceneryMapper;
import com.gzz.mapper.TicketOrderMapper;
import com.gzz.pojo.Scenery;
import com.gzz.pojo.Strategy;
import com.gzz.service.HomePageService;
import com.gzz.vo.HomePageContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HomePageServiceImpl implements HomePageService {

    @Autowired
    HomePageMapper homePageMapper;
    @Resource
    SceneryMapper sceneryMapper;

    @Override
    public HomePageContent getHomeContent()
    {
        List<Strategy> strategyList = homePageMapper.getStrategyList();
        List<Scenery> sceneryList = sceneryMapper.getTopFour();
        strategyList = strategyList.subList(0,4);
        HomePageContent homePageContent = new HomePageContent(strategyList,sceneryList);
        return homePageContent;
    }

}
