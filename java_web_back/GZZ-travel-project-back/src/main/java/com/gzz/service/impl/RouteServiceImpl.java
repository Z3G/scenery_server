package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.common.R;
import com.gzz.pojo.Route;
import com.gzz.mapper.RouteMapper;
import com.gzz.pojo.Strategy;
import com.gzz.service.ImgsRespService;
import com.gzz.service.RouteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.utils.OssUtils;
import com.gzz.vo.RouteVo;
import com.gzz.vo.StrategyDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class RouteServiceImpl extends ServiceImpl<RouteMapper, Route> implements RouteService {
	@Autowired
	ImgsRespService imgsRespService;


	// 根据id查询路线详细信息，返回封装好的routeVo给前端进行展示
	@Override
	public R<RouteVo> getDetailById(Long id) {
		// 调用mybatisplus的方法
		Route route = getById(id);
		RouteVo routeVo = new RouteVo();
		// 对象属性复制
		BeanUtils.copyProperties(route,routeVo);
		// 获取图片列表
		List<String> imgs = imgsRespService.getlistByIdAndTypeId(new StrategyDetailVo(),id, OssUtils.RouteTypeId);
		if(imgs == null){
			return R.error("数据异常");
		}
		routeVo.setRouteImags(imgs);
		return R.success(routeVo);
	}

	@Override
	public Integer getCountByCondition(String search) {
		// mybaitsplus 提供的条件查询器
		LambdaQueryWrapper<Route> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		// 模糊匹配，如果search为空则不进行条件查询
		lambdaQueryWrapper.like(search!=null,Route::getRouteName,search);

		List<Route> routes = new ArrayList<>();
		// 调用mybatisplus的list方法，传入查询器，会根据查询器的设置的条件进行查询
		routes = list(lambdaQueryWrapper);
		// 返回路线列表的个数
		return  routes.size();
	}

	// 获取路线信息列表返回给前端
	@Override
	public R<List<RouteVo>> getDetailAll(String search) {
		// 使用的是mybatisplus提供的条件查询器
		LambdaQueryWrapper<Route> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		// like模糊匹配，相当于 '路线名' like '%search%'，如果search不为空才会条件生效
		lambdaQueryWrapper.like(search!=null,Route::getRouteName,search);

		List<Route> routes = new ArrayList<>();
		// mybatisplus提供的list方法，传入条件查询器，根据这个条件进行查询
		routes = list(lambdaQueryWrapper);
		// 返回的结果列表
		List<RouteVo> res = new ArrayList<>();
		// foreach遍历列表
		routes.forEach(item->{
			RouteVo routeVo = new RouteVo();
			// 根据id查询路线详情
			routeVo = getDetailById(item.getRouteId()).getData();
			res.add(routeVo);
		});

		return R.success(res);
	}

}
