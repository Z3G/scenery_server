package com.gzz.service.impl;

import com.gzz.pojo.Lost;
import com.gzz.mapper.LostMapper;
import com.gzz.service.LostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class LostServiceImpl extends ServiceImpl<LostMapper, Lost> implements LostService {

}
