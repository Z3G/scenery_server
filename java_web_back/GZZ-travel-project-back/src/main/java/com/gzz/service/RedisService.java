package com.gzz.service;

// RedisService.java


import com.gzz.Dto.UserLikeCountDTO;
import com.gzz.Dto.UserLikesDTO;

import java.util.List;
import java.util.Set;

/**
 * 负责将数据写入Redis缓存
 */
public interface RedisService {
	/**
	 * 获取点赞状态
	 * @param infoId
	 * @param likeUserId
	 */
	Integer getLikeStatus(Long infoId, Long likeUserId);
	/**
	 * 点赞。状态为1
	 * @param infoId
	 * @param likeUserId
	 */
	void saveLiked2Redis(Long infoId, Long likeUserId);

	/**
	 * 取消点赞。将状态改变为0
	 * @param infoId
	 * @param likeUserId
	 */
	void unlikeFromRedis(Long infoId, Long likeUserId);

	/**
	 * 从Redis中删除一条点赞数据
	 * @param infoId
	 * @param likeUserId
	 */
	void deleteLikedFromRedis(Long infoId, Long likeUserId);

	/**
	 * 该内容的点赞数变化Δdelta
	 * @param infoId
	 */
	void in_decrementLikedCount(Long infoId, Integer delta);

	/**
	 * 获取Redis中存储的所有点赞数据
	 * @return
	 */
	List<UserLikesDTO> getLikedDataFromRedis();

	/**
	 * 获取Redis中存储的所有点赞数量
	 * @return
	 */
	List<UserLikeCountDTO> getLikedCountFromRedis();

	Set<String> getUpdatedStId();
}
