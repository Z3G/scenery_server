package com.gzz.service;

import com.gzz.common.R;
import com.gzz.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.UserDetailVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface UserService extends IService<User> {

	List<UserDetailVo> getUserDetailVoList(String userName, Long userId, Integer status, String userArea, Integer sex);

}
