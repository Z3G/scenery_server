package com.gzz.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzz.mapper.ImgsRespMapper;
import com.gzz.pojo.Food;
import com.gzz.mapper.FoodMapper;
import com.gzz.pojo.ImgsResp;
import com.gzz.service.FoodService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.service.ImgsRespService;
import com.gzz.utils.OssUtils;
import com.gzz.vo.FoodVo;
import com.gzz.vo.StrategyDetailVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static com.gzz.utils.OssUtils.FoodType;
import static com.gzz.utils.OssUtils.GoodTypeId;

/**
 * <p>
 *  服务实现类,美食服务实现类
 * 11
 * </p>
 *
 */
@Service
@Slf4j
public class FoodServiceImpl extends ServiceImpl<FoodMapper, Food> implements FoodService {
	@Resource
	FoodMapper foodMapper;
	@Resource
	ImgsRespMapper imgsRespMapper;
	@Autowired
	ImgsRespService imgsRespService;
	/*
	*
	* 查询美食列表
	*
	*
	* */
	@Override
	public List<Food> getFindList(Page<Food> foodPage, QueryWrapper<Food> queryWrapper) {
		// mybaits提供的分页查询
		foodMapper.selectPage(foodPage,queryWrapper);
		// 获取分页后得到的数据
		List<Food> foodList = foodPage.getRecords();
		return foodList;
	}

	@Override
	public List<FoodVo> getImage(List<Food> foodList) {
		List<FoodVo> foodVoList = new ArrayList<>();
		log.info("到这了");
		for(Food food:foodList)
		{
			// 返回给前端的美食信息
			FoodVo foodVo = new FoodVo();
			// spring提供的对象复制工具类
			BeanUtils.copyProperties(food,foodVo);
			// mybatis提供条件查询器，此次是条件查询图片表
			QueryWrapper<ImgsResp> imgsRespQueryWrapper = new QueryWrapper<>();
			imgsRespQueryWrapper.eq("st_id",food.getFoodId())
					.eq("type_id",FoodType);
			// 按照条件查询器中规定的条件查询符合条件的图片列表
			List<ImgsResp> imgsRespList = imgsRespMapper.selectList(imgsRespQueryWrapper);
			// 将ImgsResp中的图片路径加入到foodVo中
			List<String> stringList = new ArrayList<>();
			for(ImgsResp imgsResp : imgsRespList)
			{
				stringList.add(imgsResp.getImageUrl());
			}
			foodVo.setImgs(stringList);
			foodVoList.add(foodVo);
		}
		log.info("内容华为"+ foodVoList);
		return foodVoList;
	}


	// 按照id查询美食详细信息，返回给前端
	@Override
	public FoodVo getFoodVobyId(Long id) {
		Food food = new Food();
		food = getById(id);
		// 获取这个美食对应的图片列表
		List<String> strings = imgsRespService.getlistByIdAndTypeId(new StrategyDetailVo(), id, FoodType);
		FoodVo foodVo = new FoodVo();
		// spring提供的对象复制工具类
		BeanUtils.copyProperties(food,foodVo);
		// 将图片赋值给foodVo中Imgs属性
		foodVo.setImgs(strings);
		return foodVo;
	}

	@Override
	public int removeByIdAndTypeId(Long stId, Integer strategyTypeId) {
		//通过id,和typeId删除数据库的图片路径，利用mybatisplus提供的条件查询器
		LambdaQueryWrapper<ImgsResp> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(ImgsResp::getTypeId,FoodType).eq(ImgsResp::getStId,stId);
		// 按照上诉条件从图片表中删除图片
		int  r = imgsRespMapper.delete(lambdaQueryWrapper);
		return  r;
	}
	// 保存美食信息以及对应的图片列表到数据库中
	@Override
	public void saveVo(FoodVo foodVo) {
		Food food = new Food();
		// spring提供的对象复制工具类
		BeanUtils.copyProperties(foodVo,food);
		if(foodVo.getImgs()!=null) {
			for (String url : foodVo.getImgs()) {
				// 保存美食信息中的图片列表到数据库中
				ImgsResp imgsResp = new ImgsResp();
				imgsResp.setImageUrl(url);
				imgsResp.setStId(Long.valueOf(foodVo.getFoodId()));
				imgsResp.setTypeId(FoodType);
				// mybatisplus提供的保存数据到数据库的方法
				imgsRespMapper.insert(imgsResp);
			}
		}
		// 保存美食信息到数据库
		foodMapper.insert(food);
	}
}

