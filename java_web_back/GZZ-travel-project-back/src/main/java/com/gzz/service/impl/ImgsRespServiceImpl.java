package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.pojo.ImgsResp;
import com.gzz.mapper.ImgsRespMapper;
import com.gzz.service.ImgsRespService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.vo.StrategyDetailVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类 图片存储服务
 * </p>
 *
 */
@Service
public class ImgsRespServiceImpl extends ServiceImpl<ImgsRespMapper, ImgsResp> implements ImgsRespService {

	@Override
	public boolean saveList(List<String> imgs, Long stId, Integer typeId) {

		for(String url:imgs){
			ImgsResp imgsResp = new ImgsResp();
			imgsResp.setTypeId(typeId);
			imgsResp.setStId(stId);
			imgsResp.setImageUrl(url);
			boolean r = save(imgsResp);
			if(!r){
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean removeByIdAndTypeId(Long stId, Integer strategyTypeId) {
		//通过id,和typeId删除数据库的图片路径
		LambdaQueryWrapper<ImgsResp> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(ImgsResp::getTypeId,strategyTypeId).eq(ImgsResp::getStId,stId);

		boolean r = remove(lambdaQueryWrapper);
		return  r;
	}

	@Override
	public List<String> getlistByIdAndTypeId(StrategyDetailVo strategyDetailVo, Long stId, Integer strategyTypeId) {
		LambdaQueryWrapper<ImgsResp> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(ImgsResp::getTypeId,strategyTypeId).eq(ImgsResp::getStId,stId);
		List<ImgsResp> list = list(lambdaQueryWrapper);
		List<String> imgs = new ArrayList<>();
		if(strategyDetailVo.getStCoverImage()!=null){
			imgs.add(strategyDetailVo.getStCoverImage());
		}

		for(ImgsResp imgsResp :list){
			imgs.add(imgsResp.getImageUrl());
		}
		return imgs;
	}
}
