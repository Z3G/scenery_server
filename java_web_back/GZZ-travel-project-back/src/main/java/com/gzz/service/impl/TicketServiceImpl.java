package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.gzz.common.R;
import com.gzz.mapper.SceneryMapper;
import com.gzz.pojo.DefaultTicketNum;
import com.gzz.pojo.Scenery;
import com.gzz.pojo.Ticket;
import com.gzz.mapper.TicketMapper;
import com.gzz.pojo.Tickettype;
import com.gzz.service.DefaultTicketNumService;
import com.gzz.service.TicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.service.TickettypeService;
import com.gzz.utils.TimeUtils;
import com.gzz.vo.TicketListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * <p>
 *  服务实现类，门票服务类
 * </p>
 *
 */
@Service
public class TicketServiceImpl extends ServiceImpl<TicketMapper, Ticket> implements TicketService {
	@Resource
	SceneryMapper sceneryMapper;

	@Autowired
	TicketService ticketService;
	@Autowired
	DefaultTicketNumService defaultTicketNumService;
	@Autowired
	TickettypeService tickettypeService;
	@Resource
	TicketMapper ticketMapper;

	@Override
	public List<Integer> getMapForDateAndNum(Long scId) {
		List<Integer> res = new ArrayList<>();

		List<Ticket> list;

		LambdaQueryWrapper<Ticket> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(Ticket::getScId,scId).orderByAsc(Ticket::getTkDate);

		list = list(lambdaQueryWrapper);

		for(Ticket ticket : list){
			res.add(ticket.getNum());
		}

		return res;

	}
	//每天23.59添加第七天的票,自动更新今天的
	@Scheduled(cron="0 59 23 * * *")
	@Override
	public void UpdateTicketEveryday() {
		List<Long> list = sceneryMapper.getAllChargeId();
		for(Long id : list){
			LambdaQueryWrapper<Ticket> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String dd = format.format(date);

			lambdaQueryWrapper.eq(Ticket::getScId,id).eq(Ticket::getTkDate,dd);

			Ticket ticket = new Ticket();

			DefaultTicketNum dn = defaultTicketNumService.getById(id);
			int num = dn.getNum();
			ticket.setNum(num);
			ticket.setScId(id);
			Scenery scenery = sceneryMapper.selectById(id);
			ticket.setScName(scenery.getScName());
			ticket.setInitNum(num);

			Long tid = (long)Math.abs(UUID.randomUUID().hashCode());
			ticket.setTicketId(tid);
			LocalDate localDate =LocalDate.now();
			//ticket.setTkDate(localDate.plusDays(7));
			Date d = Date.from(localDate.plusDays(7).atStartOfDay(ZoneId.of("Asia/Shanghai")).toInstant());
			ticket.setTkDate(d);
			ticketService.save(ticket);
		}
		LambdaUpdateWrapper<Ticket> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String d = format.format(date);
		lambdaUpdateWrapper.eq(Ticket::getTkDate,d);
		lambdaUpdateWrapper.set(Ticket::getTkStatus,false);
		ticketService.update(lambdaUpdateWrapper);
	}

	@Override
	public R<List<TicketListVo>> getTicketAdminList(String search, Integer value, String sDate) {
		List<TicketListVo> res = new ArrayList<>();
		List<Ticket> tickets = new ArrayList<>();
		LambdaQueryWrapper<Ticket> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.like(search!=null,Ticket::getScName,search);

		if(value!=null){
			lambdaQueryWrapper.eq(Ticket::getTkStatus,value==1);
		}
		lambdaQueryWrapper.eq(sDate!=null,Ticket::getTkDate,sDate).orderByDesc(Ticket::getTkDate);
		tickets=list(lambdaQueryWrapper);
		tickets.forEach(item->{
			boolean f = true;
			if(f){
				TicketListVo ticketListVo = new TicketListVo();
				BeanUtils.copyProperties(item,ticketListVo);
				Long scId = item.getScId();
				Scenery scenery = sceneryMapper.selectById(scId);
				Integer p = scenery.getScPrice();
				LambdaQueryWrapper<Tickettype> tickettypeLambdaQueryWrapper = new LambdaQueryWrapper<>();
				tickettypeLambdaQueryWrapper.eq(Tickettype::getScId,scId);
				List<Tickettype> tickettypeList = tickettypeService.list(tickettypeLambdaQueryWrapper);
				tickettypeList.forEach(it->{
					if(it.getTypeName().equals("成人票")){
						BigDecimal sale = it.getSale();
						BigDecimal bt = sale.multiply(new BigDecimal(p.intValue()));
						int i = bt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();

						ticketListVo.setAdultPrice(Integer.valueOf(i));

					}else if(it.getTypeName().equals("学生票")){
						BigDecimal sale = it.getSale();
						BigDecimal bt = sale.multiply(new BigDecimal(p.intValue()));
						int i = bt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
						ticketListVo.setStudentPrice(Integer.valueOf(i));
					}else{
						ticketListVo.setChildPrice(p);
					}
				});
				res.add(ticketListVo);
			}

		});
		//lambdaQueryWrapper.eq(sDate)
		return  R.success(res);
	}

	@Override
	public Integer getCount(String search, Integer value, String sDate) {
		List<TicketListVo> res = new ArrayList<>();
		List<Ticket> tickets = new ArrayList<>();
		LambdaQueryWrapper<Ticket> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.like(search!=null,Ticket::getScName,search);

		if(value!=null){
			lambdaQueryWrapper.eq(Ticket::getTkStatus,value==1);
		}
		lambdaQueryWrapper.eq(sDate!=null,Ticket::getTkDate,sDate);
		tickets=list(lambdaQueryWrapper);
		return tickets.size();
	}
	// 当添加了新的游玩景点项目时候，自动设置他的七天的门票数量，门票类型
	@Override
	public void addByScenery(Scenery scenery) {

		Long id = scenery.getScId();
		String scName = scenery.getScName();
		Integer scPrice = scenery.getScPrice();
		for(int i=0;i<7;i++){
			Ticket ticket = new Ticket();
			List<DefaultTicketNum> dn = defaultTicketNumService.list();
			int num = dn.get(0).getNum();
			ticket.setNum(num);
			ticket.setScId(id);
			ticket.setScName(scName);
			ticket.setInitNum(num);

			Long tid = (long)Math.abs(UUID.randomUUID().hashCode());
			ticket.setTicketId(tid);

			LocalDate localDate =LocalDate.now();

			Date d = Date.from(localDate.plusDays(i).atStartOfDay(ZoneId.of("Asia/Shanghai")).toInstant());
			ticket.setTkDate(d);

			ticketService.save(ticket);
		}

		for(int i=1;i<=3;i++){
			Tickettype tickettype = new Tickettype();
			tickettype.setScId(id);
			tickettype.setTypeId(i);
			if(i==1){
				tickettype.setSale(new BigDecimal(2));

				tickettype.setTypeName("成人票");
			}else if(i==2){
				tickettype.setTypeName("儿童票");

				tickettype.setSale(new BigDecimal(1));
				tickettype.setScId(id);
			}else{
				tickettype.setTypeName("学生票");
				float f = 1.5f;
				tickettype.setSale(new BigDecimal(f));
				tickettype.setScId(id);
			}
			tickettypeService.save(tickettype);

		}

	}

	@Override
	public void removeByscId(Long scId) {
		ticketMapper.deletebyscId(scId);
	}

	@Override
	public Ticket creatTicketByTkDateAndScId(Long scI, Date d) {
		Scenery scenery = sceneryMapper.selectById(scI);
		DefaultTicketNum df = defaultTicketNumService.getById(scI);
		Ticket ticket = new Ticket();

		Long tid =(long)Math.abs(cn.hutool.core.lang.UUID.randomUUID().hashCode());

		ticket.setTicketId(tid);
		ticket.setInitNum(df.getNum());
		ticket.setNum(df.getNum());
		ticket.setSellNum(0);
		ticket.setTkDate(d);
		ticket.setScName(scenery.getScName());
		ticket.setScId(scI);
		save(ticket);
		return ticket;
	}

}
