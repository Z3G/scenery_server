package com.gzz.service;

import com.gzz.pojo.GoodsOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface GoodsOrderService extends IService<GoodsOrder> {

}
