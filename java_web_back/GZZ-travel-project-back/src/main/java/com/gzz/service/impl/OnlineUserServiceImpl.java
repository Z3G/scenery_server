package com.gzz.service.impl;

import com.gzz.pojo.OnlineUser;
import com.gzz.mapper.OnlineUserMapper;
import com.gzz.service.OnlineUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class OnlineUserServiceImpl extends ServiceImpl<OnlineUserMapper, OnlineUser> implements OnlineUserService {

}
