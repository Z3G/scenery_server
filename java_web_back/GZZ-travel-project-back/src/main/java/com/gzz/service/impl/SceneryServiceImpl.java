package com.gzz.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.common.R;
import com.gzz.pojo.ImgsResp;
import com.gzz.pojo.Scenery;
import com.gzz.mapper.SceneryMapper;
import com.gzz.pojo.Tickettype;
import com.gzz.service.ImgsRespService;
import com.gzz.service.SceneryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.service.TicketOrderService;
import com.gzz.service.TickettypeService;
import com.gzz.utils.OssUtils;
import com.gzz.vo.SenceryDetailVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class SceneryServiceImpl extends ServiceImpl<SceneryMapper, Scenery> implements SceneryService {

	@Autowired
	TickettypeService tickettypeService;
	@Autowired
	ImgsRespService imgsRespService;
	@Resource
	SceneryMapper sceneryMapper;

	@Override
	public R<SenceryDetailVo> getDetailbyId(Long scId) {
		LambdaQueryWrapper<Tickettype> tickettypeLambdaQueryWrapper = new LambdaQueryWrapper<>();
		tickettypeLambdaQueryWrapper.eq(Tickettype::getScId,scId);
		List<Tickettype> typelist = tickettypeService.list(tickettypeLambdaQueryWrapper);

		LambdaQueryWrapper<ImgsResp> imgsRespLambdaQueryWrapper = new LambdaQueryWrapper<>();
		imgsRespLambdaQueryWrapper.eq(ImgsResp::getTypeId, OssUtils.ViewTypeId).eq(ImgsResp::getStId,scId);
		List<ImgsResp> imgsRespList = imgsRespService.list(imgsRespLambdaQueryWrapper);

		List<String> imgs  = new ArrayList<>();

		imgsRespList.forEach((item)->{
			imgs.add(item.getImageUrl());
		});

		Scenery scenery = getById(scId);

		SenceryDetailVo senceryDetailVo = new SenceryDetailVo();

		BeanUtils.copyProperties(scenery,senceryDetailVo);

		senceryDetailVo.setScImages(imgs);

		senceryDetailVo.setTicketTypes(typelist);

		return  R.success(senceryDetailVo);

	}

	@Override
	public R<List<SenceryDetailVo>> getDetailAll(String search,Integer value) {
		List<SenceryDetailVo> res = new ArrayList<>();
		List<Scenery> scList = new ArrayList<>();
		LambdaQueryWrapper<Scenery> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.like(search!=null,Scenery::getScName,search);
		lambdaQueryWrapper.eq(value!=null,Scenery::getScStatus,value);
		scList = list(lambdaQueryWrapper);
		scList.forEach(item->{
			Long scId = item.getScId();
			SenceryDetailVo senceryDetailVo = new SenceryDetailVo();
			senceryDetailVo = getDetailbyId(scId).getData();
			res.add(senceryDetailVo);
		});

		return R.success(res);
	}

	@Override
	public Integer getTotalByCondition(String search, Integer value) {
		List<Scenery> scList = new ArrayList<>();
		LambdaQueryWrapper<Scenery> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.like(search!=null,Scenery::getScName,search);
		lambdaQueryWrapper.eq(value!=null,Scenery::getScStatus,value);
		scList = list(lambdaQueryWrapper);
		return  scList.size();
	}

}
