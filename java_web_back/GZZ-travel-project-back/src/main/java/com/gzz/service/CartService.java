package com.gzz.service;

import com.gzz.pojo.Cart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface CartService extends IService<Cart> {

}
