package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gzz.mapper.ImgsRespMapper;

import com.gzz.mapper.SystemMessageMapper;
import com.gzz.pojo.ImgsResp;

import com.gzz.pojo.SystemMessage;
import com.gzz.pojo.UserAdvise;
import com.gzz.mapper.UserAdviseMapper;

import com.gzz.service.UserAdviseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.vo.AdviseVo;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static com.gzz.utils.OssUtils.LostTypeId;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class UserAdviseServiceImpl extends ServiceImpl<UserAdviseMapper, UserAdvise> implements UserAdviseService {

    @Resource
    ImgsRespMapper imgsRespMapper;
    @Resource
    SystemMessageMapper systemMessageMapper;
    @Override
    public List<AdviseVo> getVo(List<UserAdvise> userAdviseList) {
        List<AdviseVo> adviseVoList = new ArrayList<>();
        for(UserAdvise userAdvise : userAdviseList)
        {
            AdviseVo adviseVo = new AdviseVo();
            BeanUtils.copyProperties(userAdvise,adviseVo);
            QueryWrapper<ImgsResp> imgsRespQueryWrapper = new QueryWrapper<>();
            imgsRespQueryWrapper.eq("st_id",userAdvise.getId())
                    .eq("type_id",LostTypeId);
            List<ImgsResp> imgsRespList = imgsRespMapper.selectList(imgsRespQueryWrapper);
            List<String> stringList  = new ArrayList<>();
            for(ImgsResp imgsResp : imgsRespList)
            {
                stringList.add(imgsResp.getImageUrl());
            }
            adviseVo.setImgs(stringList);
            QueryWrapper<SystemMessage> systemMessageQueryWrapper = new QueryWrapper<>();
            systemMessageQueryWrapper.eq("advice_id",userAdvise.getId());
            SystemMessage systemMessage = systemMessageMapper.selectOne(systemMessageQueryWrapper);
            if(systemMessage!=null) {
                adviseVo.setBackContext(systemMessage.getContext());
                adviseVo.setTitle(systemMessage.getTitle());
                adviseVo.setCreateTime(systemMessage.getCreateTime());
            }
            adviseVoList.add(adviseVo);
        }
        return adviseVoList;


    }
}
