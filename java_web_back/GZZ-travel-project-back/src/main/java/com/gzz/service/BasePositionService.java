package com.gzz.service;

import com.gzz.pojo.BasePosition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface BasePositionService extends IService<BasePosition> {

}
