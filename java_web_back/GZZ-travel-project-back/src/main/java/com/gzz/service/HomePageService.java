package com.gzz.service;

import com.gzz.vo.HomePageContent;

public interface HomePageService {
    public HomePageContent getHomeContent();

}
