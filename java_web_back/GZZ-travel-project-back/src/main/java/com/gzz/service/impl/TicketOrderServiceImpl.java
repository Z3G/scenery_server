package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.gzz.common.R;
import com.gzz.mapper.TicketMapper;
import com.gzz.mapper.TikcetOrderDetailMapper;
import com.gzz.pojo.Ticket;
import com.gzz.pojo.TicketOrder;
import com.gzz.mapper.TicketOrderMapper;
import com.gzz.pojo.TikcetOrderDetail;
import com.gzz.service.TicketOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.service.TikcetOrderDetailService;
import com.gzz.vo.TicketOrderDetailVo;
import com.gzz.vo.TicketOrderVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类，门票订单
 * </p>
 *
 */
@Service
public class TicketOrderServiceImpl extends ServiceImpl<TicketOrderMapper, TicketOrder> implements TicketOrderService {

	@Resource
	TicketMapper ticketMapper;
	@Resource
	TikcetOrderDetailMapper tikcetOrderDetailMapper;
	@Autowired
	TikcetOrderDetailService tikcetOrderDetailService;

	@Override
	@Transactional(rollbackFor = RuntimeException.class)
	public void delOrderTimeoutById(Long id) {
		TicketOrder ticketOrder = getById(id);
		Long TicketId = ticketOrder.getTicketId();
		Integer num = ticketOrder.getNum();


		Ticket ticket = ticketMapper.selectById(TicketId);

		ticket.setNum(ticket.getNum()+num);
		ticket.setSellNum(ticket.getSellNum()-num);
		int r = ticketMapper.updateById(ticket);

		if(r<=0){
			throw new RuntimeException("更新票失败在删除超时订单相关信息时");
		}
		LambdaUpdateWrapper<TikcetOrderDetail> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
		lambdaUpdateWrapper.eq(TikcetOrderDetail::getOrderId,id);

		int dr = tikcetOrderDetailMapper.delete(lambdaUpdateWrapper);
		if(dr<=0){
			throw new RuntimeException("更订单明细表失败在删除超时订单相关信息时");
		}

		boolean od = removeById(id);

		if(!od){
			throw new RuntimeException("删除超时订单失败");
		}


	}

	@Override
	public R<TicketOrderVo> getDetailById(Long orderId) {
		TicketOrderVo ticketOrderVo = new TicketOrderVo();

		TicketOrder ticketOrder = getById(orderId);

		BeanUtils.copyProperties(ticketOrder,ticketOrderVo);
		//获得订单详情表的Vo
		List<TicketOrderDetailVo> ticketOrderDetailVoList = tikcetOrderDetailService.getAllDetailVo(orderId);

		ticketOrderVo.setOrderDetail(ticketOrderDetailVoList);

		return  R.success(ticketOrderVo);
	}
	@Override
	public R<List<TicketOrderVo>> getDetailAll(String search, Integer status, Long userId, Long orderId) {
		LambdaQueryWrapper<TicketOrder> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		lambdaQueryWrapper.like(search!=null,TicketOrder::getScName,search);
		lambdaQueryWrapper.eq(status!=null,TicketOrder::getStatus,status)
				.eq(userId!=null,TicketOrder::getUserId,userId)
				.eq(orderId!=null,TicketOrder::getOrderId,orderId);
		List<TicketOrder> ticketOrders = list(lambdaQueryWrapper);

		List<TicketOrderVo> res = new ArrayList<>();

		ticketOrders.forEach(item->{
			Long orderId1 = item.getOrderId();
			res.add(getDetailById(orderId1).getData());
		});

		return R.success(res);
	}


	@Override
	public Integer getTotalbyCondition(String search, Integer status, Long userId, Long orderId) {
		LambdaQueryWrapper<TicketOrder> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		lambdaQueryWrapper.like(search!=null,TicketOrder::getScName,search);
		lambdaQueryWrapper.eq(status!=null,TicketOrder::getStatus,status)
				.eq(userId!=null,TicketOrder::getUserId,userId)
				.eq(orderId!=null,TicketOrder::getOrderId,orderId);
		List<TicketOrder> ticketOrders = list(lambdaQueryWrapper);

		return ticketOrders.size();
	}
}
