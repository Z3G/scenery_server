package com.gzz.service;

import com.gzz.pojo.PlayTable;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.ProvinceNumVo;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface PlayTableService extends IService<PlayTable> {
	public List<ProvinceNumVo> getProvinceNumVo();
}
