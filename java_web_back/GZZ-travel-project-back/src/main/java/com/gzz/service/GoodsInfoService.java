package com.gzz.service;

import com.gzz.pojo.GoodsInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.FoodVo;
import com.gzz.vo.GoodsVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface GoodsInfoService extends IService<GoodsInfo> {

    List<GoodsVo> getVo(List<GoodsInfo> goodsInfoList);

    int removeByIdAndTypeId(Long stId, Integer strategyTypeId);


    void saveVo(GoodsVo goodsVo);
}
