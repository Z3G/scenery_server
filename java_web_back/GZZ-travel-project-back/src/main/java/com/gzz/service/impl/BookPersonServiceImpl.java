package com.gzz.service.impl;

import com.gzz.pojo.BookPerson;
import com.gzz.mapper.BookPersonMapper;
import com.gzz.service.BookPersonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类 ，预订人信息
 * </p>
 *
 */
@Service
public class BookPersonServiceImpl extends ServiceImpl<BookPersonMapper, BookPerson> implements BookPersonService {

}
