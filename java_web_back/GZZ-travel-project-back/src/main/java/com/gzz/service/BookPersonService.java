package com.gzz.service;

import com.gzz.pojo.BookPerson;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类 游玩人
 * </p>
 *
 */
public interface BookPersonService extends IService<BookPerson> {

}
