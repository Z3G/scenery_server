package com.gzz.service;

import com.gzz.pojo.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface CommentService extends IService<Comment> {

}
