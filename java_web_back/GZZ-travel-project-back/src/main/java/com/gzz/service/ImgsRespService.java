package com.gzz.service;

import com.gzz.pojo.ImgsResp;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.StrategyDetailVo;

import java.util.List;

/**
 * <p>
 *  服务类 图片入库服务，图片存储服务
 * </p>
 *
 */
public interface ImgsRespService extends IService<ImgsResp> {

	boolean saveList(List<String> imgs, Long id, Integer typeId);

	boolean removeByIdAndTypeId(Long stId, Integer strategyTypeId);

	List<String> getlistByIdAndTypeId(StrategyDetailVo strategyDetailVo, Long stId, Integer strategyTypeId);
}
