package com.gzz.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzz.pojo.Food;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.FoodVo;

import java.util.List;

/**
 * <p>
 *  服务类，美食信息服务接口
 * </p>
 *
 */
public interface FoodService extends IService<Food> {


	int removeByIdAndTypeId(Long stId, Integer strategyTypeId);

	void saveVo(FoodVo foodVo);
	FoodVo getFoodVobyId(Long id);

	List<Food> getFindList(Page<Food> foodPage, QueryWrapper<Food> queryWrapper);

	List<FoodVo> getImage(List<Food> foodList);
}

