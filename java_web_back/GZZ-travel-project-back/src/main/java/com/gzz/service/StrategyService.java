package com.gzz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzz.common.R;
import com.gzz.pojo.Strategy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.StrategyDetailVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface StrategyService extends IService<Strategy> {

	List<Strategy> getListByUserId(Long id);

	R<Page> getListByTitle(String keyword, int pageNo, int pageSize);

	R<StrategyDetailVo> getStrategyDetail(Long stId);

	R<List<StrategyDetailVo>> getDetailAll(String search, Long userId, Integer s_status, Integer s_isPublic);

	Integer getTotalbyCondition(String search, Long userId, Integer s_status, Integer s_isPublic);
}
