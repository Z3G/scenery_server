package com.gzz.service;

import com.gzz.pojo.Strategy;
import com.gzz.pojo.UserSc;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 收藏记录表 服务类
 * </p>
 *
 */
public interface UserScService extends IService<UserSc> {

	List<Strategy> listByUserId(Long id);
}
