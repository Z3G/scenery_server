package com.gzz.service;

import com.gzz.common.R;
import com.gzz.pojo.Activity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.ActivityVo;
import com.gzz.vo.SenceryDetailVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 * 使用mybatisplus框架，提供了一系列增删改查的函数，IService接口是mybatisplus提供的
 */
public interface ActivityService extends IService<Activity> {
	R<ActivityVo> getDetailbyId(Long actId);

	R<List<ActivityVo>> getDetailAll(String search, Integer value);
}
