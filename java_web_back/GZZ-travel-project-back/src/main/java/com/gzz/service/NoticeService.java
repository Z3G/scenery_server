package com.gzz.service;

import com.gzz.pojo.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface NoticeService extends IService<Notice> {

}
