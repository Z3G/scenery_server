package com.gzz.service.impl;

import com.gzz.pojo.GoodsOrder;
import com.gzz.mapper.GoodsOrderMapper;
import com.gzz.service.GoodsOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 *
 *
 * </p>
 *
 */
@Service
public class GoodsOrderServiceImpl extends ServiceImpl<GoodsOrderMapper, GoodsOrder> implements GoodsOrderService {

}
