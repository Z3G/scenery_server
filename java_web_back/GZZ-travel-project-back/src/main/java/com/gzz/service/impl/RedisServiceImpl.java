package com.gzz.service.impl;

import com.gzz.Dto.UserLikeCountDTO;
import com.gzz.Dto.UserLikesDTO;
import com.gzz.service.RedisService;
import com.gzz.utils.RedisCache;
import com.gzz.utils.RedisKeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
@Service
public class RedisServiceImpl implements RedisService {

	@Autowired
	RedisCache redisCache;

	@Override
	public Integer getLikeStatus(Long infoId, Long likeUserId) {
		String key = RedisKeyUtils.getLikedKey(infoId,likeUserId);

		Map<String,Integer> map = new HashMap<>();

		map = redisCache.getCacheMap(RedisKeyUtils.MAP_KEY_USER_INFO);

		return map.get(key);
	}

	@Override
	public void saveLiked2Redis(Long infoId, Long likeUserId) {
		String key = RedisKeyUtils.getLikedKey(infoId,likeUserId);
		Integer status = 1;

		redisCache.setCacheMapValue(RedisKeyUtils.MAP_KEY_USER_INFO,key,status);

		String NumKey = RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT;
		Integer curNum = 0;
		if(redisCache.getCacheMapValue(NumKey,infoId.toString())!=null){
			curNum = redisCache.getCacheMapValue(NumKey,infoId.toString());
		}
		//存储infoId中点赞数量变化量，与数据库中数据作对比
		redisCache.setCacheMapValue(NumKey,infoId.toString(),curNum+1);
	}

	@Override
	public void unlikeFromRedis(Long infoId, Long likeUserId) {
		String key = RedisKeyUtils.getLikedKey(infoId,likeUserId);
		Integer status = 0;

		redisCache.setCacheMapValue(RedisKeyUtils.MAP_KEY_USER_INFO,key,status);

		String NumKey = RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT;
		Integer curNum = 0;
		if(redisCache.getCacheMapValue(NumKey,infoId.toString())!=null){
			curNum = redisCache.getCacheMapValue(NumKey,infoId.toString());
		}
		//存储infoId中点赞数量变化量，与数据库中数据作对比
		redisCache.setCacheMapValue(NumKey,infoId.toString(),curNum-1);
	}

	@Override
	public void deleteLikedFromRedis(Long infoId, Long likeUserId) {
		String key = RedisKeyUtils.getLikedKey(infoId,likeUserId);
		redisCache.delCacheMapValue(RedisKeyUtils.MAP_KEY_USER_INFO,key);
	}



	@Override
	public void in_decrementLikedCount(Long infoId, Integer delta) {

	}

	@Override
	public List<UserLikesDTO> getLikedDataFromRedis() {
		List<UserLikesDTO> list = new ArrayList<>();
		Map<String,Integer> map;
		map = redisCache.getCacheMap(RedisKeyUtils.MAP_KEY_USER_INFO);


		if(map==null || map.isEmpty()){
			return null;
		}
		Set<String> keys = map.keySet();
		for(String key : keys){
			UserLikesDTO userLikesDTO = new UserLikesDTO();
			String InfoId = key.substring(0,key.lastIndexOf(":"));
			userLikesDTO.setInfoId(Long.valueOf(InfoId));
			int index = key.lastIndexOf(":");
			String userId = key.substring(index+1,key.length());
			userLikesDTO.setLikeUserId(Long.valueOf(userId));
			userLikesDTO.setStatus(map.get(key));
			list.add(userLikesDTO);
		}
		return  list;

	}

	@Override
	public List<UserLikeCountDTO> getLikedCountFromRedis() {

		Map<String,Integer> map = redisCache.getCacheMap(RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT);
		Set<String> keys = map.keySet();
		List<UserLikeCountDTO> list = new ArrayList<>();
		for(String key:keys){
			UserLikeCountDTO likeCountDTO = new UserLikeCountDTO();
			likeCountDTO.setInfoId(Long.valueOf(key));
			likeCountDTO.setLikeCount(map.get(key));
			list.add(likeCountDTO);
		}
		return list;
	}

	@Override
	public Set<String> getUpdatedStId() {
		Set<String> set = new HashSet<>();
		Map<String,Integer> map;
		map = redisCache.getCacheMap(RedisKeyUtils.MAP_KEY_USER_LIKED_COUNT);
		set = map.keySet();
		return  set;
	}


}
