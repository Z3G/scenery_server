package com.gzz.service.impl;

import com.gzz.pojo.DefaultTicketNum;
import com.gzz.mapper.DefaultTicketNumMapper;
import com.gzz.service.DefaultTicketNumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类，门票数量配置服务类
 * </p>
 *
 */
@Service
public class DefaultTicketNumServiceImpl extends ServiceImpl<DefaultTicketNumMapper, DefaultTicketNum> implements DefaultTicketNumService {

}
