package com.gzz.service.impl;

import com.gzz.pojo.Tickettype;
import com.gzz.mapper.TickettypeMapper;
import com.gzz.service.TickettypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类，门票类型
 * </p>
 *
 */
@Service
public class TickettypeServiceImpl extends ServiceImpl<TickettypeMapper, Tickettype> implements TickettypeService {

}
