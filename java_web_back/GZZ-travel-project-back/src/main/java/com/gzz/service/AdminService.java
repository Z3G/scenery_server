package com.gzz.service;

import com.gzz.pojo.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface AdminService extends IService<Admin> {

}
