package com.gzz.service;

import com.gzz.pojo.SystemMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface SystemMessageService extends IService<SystemMessage> {

}
