package com.gzz.service;

import com.gzz.pojo.UserLikes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 点赞记录表 服务类
 * </p>
 *
 */
public interface UserLikesService extends IService<UserLikes> {
	 void updateFromRedis();
}
