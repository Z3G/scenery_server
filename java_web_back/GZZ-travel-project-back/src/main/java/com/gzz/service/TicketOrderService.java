package com.gzz.service;

import com.gzz.Dto.TicketOrderDto;
import com.gzz.common.R;
import com.gzz.pojo.TicketOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.pojo.TikcetOrderDetail;
import com.gzz.vo.TicketOrderVo;

import java.util.List;

/**
 * <p>
 *  服务类，门票订单服务接口
 * </p>
 *
 */
public interface TicketOrderService extends IService<TicketOrder> {
	void delOrderTimeoutById(Long id);

	R<List<TicketOrderVo>> getDetailAll(String search, Integer status, Long userId, Long orderId);
	R<TicketOrderVo> getDetailById(Long orderId);

	Integer getTotalbyCondition(String search, Integer status, Long userId, Long orderId);
}
