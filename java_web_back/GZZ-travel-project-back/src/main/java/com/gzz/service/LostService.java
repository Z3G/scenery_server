package com.gzz.service;

import com.gzz.pojo.Lost;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface LostService extends IService<Lost> {

}
