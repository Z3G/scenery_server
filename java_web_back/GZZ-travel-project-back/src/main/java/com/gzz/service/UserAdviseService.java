package com.gzz.service;

import com.gzz.pojo.UserAdvise;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.AdviseVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface UserAdviseService extends IService<UserAdvise> {

    List<AdviseVo> getVo(List<UserAdvise> userAdviseList);
}
