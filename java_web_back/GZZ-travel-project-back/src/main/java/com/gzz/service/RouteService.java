package com.gzz.service;

import com.gzz.common.R;
import com.gzz.pojo.Route;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzz.vo.RouteVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface RouteService extends IService<Route> {

	R<List<RouteVo>> getDetailAll(String search);
	R<RouteVo> getDetailById(Long id);
	Integer getCountByCondition(String search);
}
