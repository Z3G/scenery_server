package com.gzz.service.impl;

import com.gzz.pojo.PlayTable;
import com.gzz.mapper.PlayTableMapper;
import com.gzz.service.PlayTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzz.utils.IDCardsUitls;
import com.gzz.vo.ProvinceNumVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类，游玩人数据
 * </p>
 *
 */
@Service
public class PlayTableServiceImpl extends ServiceImpl<PlayTableMapper, PlayTable> implements PlayTableService {

	@Override
	public List<ProvinceNumVo> getProvinceNumVo() {
		List<PlayTable> list = new ArrayList<>();
		list = list();
		List<ProvinceNumVo> res = new ArrayList<>();
		HashMap<Integer,Integer> map = new HashMap<>();
		list.forEach(item->{
			String twoString = item.getIdCard().substring(0,2);
			Integer t = Integer.valueOf(twoString);
			if(map.get(t)==null){
				map.put(t,1);
			}else{
				map.put(t,map.get(t)+1);
			}
		});
		for(Integer integer : map.keySet()){
			ProvinceNumVo provinceNumVo = new ProvinceNumVo();
			provinceNumVo.setName(IDCardsUitls.province.get(integer));
			provinceNumVo.setValue(map.get(integer));
			res.add(provinceNumVo);
		}
		return res;
	}
}
