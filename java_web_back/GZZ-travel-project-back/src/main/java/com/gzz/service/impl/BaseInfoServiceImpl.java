package com.gzz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.common.R;
import com.gzz.pojo.BaseInfo;
import com.gzz.mapper.BaseInfoMapper;
import com.gzz.service.BaseInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class BaseInfoServiceImpl extends ServiceImpl<BaseInfoMapper, BaseInfo> implements BaseInfoService {

	@Override
	public R<List<BaseInfo>> getBaseInfoListbyAdmin(String search, Long userId, Integer status) {
		LambdaQueryWrapper<BaseInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(userId!=null,BaseInfo::getUserId,userId)
				.like(search!=null, BaseInfo::getGoodsName,search)
				.eq(status!=null,BaseInfo::getTab,status);
		List<BaseInfo> list = list(lambdaQueryWrapper);

		return R.success(list);

	}
}
