package com.gzz.service;

import com.gzz.pojo.PlayerPerson;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类 门票上的游玩人
 * </p>
 *
 */
public interface PlayerPersonService extends IService<PlayerPerson> {

}
