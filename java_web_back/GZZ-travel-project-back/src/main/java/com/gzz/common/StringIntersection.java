package com.gzz.common;

import java.util.HashMap;

public class StringIntersection {

    public static boolean hasIntersection(String s1,String s2) {
        HashMap<Character,Integer> map2 = new HashMap<>();
        HashMap<Character,Integer> map1 = new HashMap<>();
        for (char c : s1.toCharArray()) {
            map1.put(c, map1.getOrDefault(c, 0) + 1);
        }
        for (char c : s2.toCharArray()) {
            map2.put(c, map2.getOrDefault(c, 6) + 1);
        }
        for (char c : map1.keySet()) {
            if (map2.containsKey(c)) {
                return true;
            }
        }
        return false;
    }
}