package com.gzz.common;

import com.gzz.pojo.BaseInfo;
import com.gzz.service.BaseInfoService;
import com.gzz.utils.RabbitMqUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyRabbitListener {
	String s = RabbitMqUtils.order_dlx_queue;

	@Autowired
	BaseInfoService baseInfoService;

	@RabbitListener(queues = "dlx.queue")
	public void listenSimpleQueue(Long orderId) throws InterruptedException {
		BaseInfo baseInfo = baseInfoService.getById(orderId);
		if(baseInfo.getTab()==1){
			baseInfoService.removeById(orderId);
		}
	}
}
