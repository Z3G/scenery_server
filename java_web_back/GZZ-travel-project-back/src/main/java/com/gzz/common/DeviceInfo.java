package com.gzz.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjc12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceInfo {
	private String clientType;
	private String osType;
	private String ipAddress;
	private String browserType;

}
