package com.gzz.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLikeCountDTO {
	private Long infoId;
	private Integer likeCount;
}
