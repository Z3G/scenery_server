package com.gzz.Dto;

import lombok.Data;

@Data
public class CartReception {

     Long cartId;
     Long goodsId;
     boolean goodsSelected;
}
