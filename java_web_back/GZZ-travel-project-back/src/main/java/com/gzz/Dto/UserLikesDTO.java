package com.gzz.Dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLikesDTO {
	private Long infoId;
	private Long likeUserId;
	private Integer status;
	private Date updateTime;
}

