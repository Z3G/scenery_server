package com.gzz.Dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gzz.pojo.BookPerson;
import com.gzz.pojo.PlayerPerson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
// 门票订单dto
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TicketOrderDto {
	/*
	* var postData = {
						bookPerson:this.bookPeople[this.bookIndex],
						playerPerson:this.playPeople,
						scId:this.scId,
						date:date1,
						typeId:this.typeId,
						scName:this.scName,
						num:this.cnt,
						price:this.price
					}
	*
	* */

	private String scName;

	private BookPerson bookPerson;

	private List<PlayerPerson> playerPerson;

	private Long scId;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh", timezone = "GMT+8")
	private Date date;

	private Integer num;

	private BigDecimal price;

	private Integer typeId;



}
