package com.gzz;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class main {

	public static void main(String[] args) {
		AutoGenerator mpg = new AutoGenerator();

		// 全局配置
		GlobalConfig gc = new GlobalConfig();
		String projectPath = System.getProperty("user.dir");
		gc.setOutputDir(projectPath + "/src/main/java");//设置代码生成路径
		gc.setFileOverride(true);//是否覆盖以前文件
		gc.setOpen(false);//是否打开生成目录
		gc.setAuthor("周佳成");//设置项目作者名称
		gc.setIdType(IdType.AUTO);//设置主键策略
		gc.setBaseResultMap(true);//生成基本ResultMap
		gc.setBaseColumnList(true);//生成基本ColumnList
		gc.setServiceName("%sService");//去掉服务默认前缀
		gc.setDateType(DateType.ONLY_DATE);//设置时间类型
		mpg.setGlobalConfig(gc);

		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setDbType(DbType.MYSQL);
		dsc.setUrl("jdbc:mysql://localhost:3306/gzz_project_database?useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8");
		dsc.setDriverName("com.mysql.cj.jdbc.Driver");
		dsc.setUsername("root");
		dsc.setPassword("zjc1226");
		mpg.setDataSource(dsc);

		PackageConfig pc = new PackageConfig();
		pc.setParent("com.gzz");
		pc.setMapper("mapper");
		pc.setXml("mapper.xml");
		pc.setEntity("pojo");
		pc.setService("service");
		pc.setServiceImpl("service.impl");
		pc.setController("controller");
		mpg.setPackageInfo(pc);

		StrategyConfig sc = new StrategyConfig();
		sc.setInclude("comment");
		sc.setNaming(NamingStrategy.underline_to_camel);
		sc.setColumnNaming(NamingStrategy.underline_to_camel);
		sc.setEntityLombokModel(true);//自动lombok
		sc.setRestControllerStyle(true);
		sc.setControllerMappingHyphenStyle(true);
		mpg.setStrategy(sc);

		mpg.execute();

	}
}
