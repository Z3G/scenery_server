package com.gzz.controller;


import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.common.R;
import com.gzz.mapper.StrategyMapper;
import com.gzz.mapper.UserScMapper;
import com.gzz.pojo.Strategy;
import com.gzz.pojo.UserLikes;
import com.gzz.pojo.UserSc;
import com.gzz.service.StrategyService;
import com.gzz.service.UserScService;
import com.gzz.utils.JWTUtils;
import com.gzz.vo.StrategyListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 收藏记录表 前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/user-sc")
public class UserScController {

	@Autowired
	UserScService userScService;

	@Resource
	StrategyMapper strategyMapper;

	@Autowired
	StrategyService strategyService;

	@Resource
	UserScMapper userScMapper;

	@GetMapping("/list")
	public R<List<StrategyListVo>> list(HttpServletRequest request){
		String token = request.getHeader("token");
		//获取用户id
		Long id = JWTUtils.parseToken(token);

		List<Strategy> strategyList = userScService.listByUserId(id);

		List<StrategyListVo> listVoList = new ArrayList<>();
		for(Strategy item:strategyList){
			StrategyListVo strategyListVo = new StrategyListVo();
			BeanUtils.copyProperties(item,strategyListVo);
			listVoList.add(strategyListVo);
		}

		return R.success(listVoList);
	}
	@GetMapping("/del")
	public R<String> del(HttpServletRequest request, HttpServletResponse response, Long stId){
		String token = request.getHeader("token");
		if(StrUtil.isBlank(token)){
			response.setStatus(401);
			return R.error("失败");
		}
		//获取用户id
		Long id = JWTUtils.parseToken(token);
		userScMapper.updateStatusByUserIdAndstId(id,stId,0);
		strategyMapper.updateScById(stId,-1);
		return R.success("成功");
	}



	@GetMapping("/getSc")
	public R<String> getSc(HttpServletRequest request,Long stId){
		String token = request.getHeader("token");
		//获取用户id
		Long id = JWTUtils.parseToken(token);

		UserSc userSc;

		LambdaQueryWrapper<UserSc> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		lambdaQueryWrapper.eq(UserSc::getUserId,id).eq(UserSc::getStId,stId);

		userSc = userScService.getOne(lambdaQueryWrapper);

		if(userSc==null||userSc.getStatus()==0){
			return R.success("false");
		}
		if(userSc.getStatus() == 1){
			return R.success("true");
		}
		return R.error("数据异常");
	}

	@GetMapping("/updateSc")
	public R<String> updateSc(HttpServletRequest request,Long stId,boolean status){
		String token = request.getHeader("token");
		//获取用户id
		Long id = JWTUtils.parseToken(token);

		UserSc userSc;
		Integer st = -1;
		if(status){
			st = 1;
		}

		LambdaQueryWrapper<UserSc> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		lambdaQueryWrapper.eq(UserSc::getUserId,id).eq(UserSc::getStId,stId);

		userSc = userScService.getOne(lambdaQueryWrapper);


		if(userSc==null){
			userSc = new UserSc();
			Long Scid = (long) Math.abs(UUID.randomUUID().hashCode());
			userSc.setId(Scid);
			userSc.setUserId(id);
			userSc.setCreateTime(new Date());
			userSc.setUpdateTime(new Date());
			userSc.setStId(stId);
			userSc.setStatus(st);
			userScService.save(userSc);
		}else{
			if(status){
				userSc.setStatus(1);
			}else {
				userSc.setStatus(0);
			}
			userScService.updateById(userSc);
		}
		strategyMapper.updateScById(stId,st);

		return R.success("success");


	}





}

