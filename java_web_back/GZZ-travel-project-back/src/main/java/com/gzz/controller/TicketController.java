package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.common.R;
import com.gzz.mapper.TickettypeMapper;
import com.gzz.pojo.Scenery;
import com.gzz.pojo.Ticket;
import com.gzz.pojo.Tickettype;
import com.gzz.service.SceneryService;
import com.gzz.service.TicketService;
import com.gzz.vo.StrategyDetailVo;
import com.gzz.vo.TicketListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器 门票操作服务
 * </p>
 *
 */
@RestController
@RequestMapping("/api/ticket")
public class TicketController {
	@Autowired
	TicketService ticketService;

	@Autowired
	SceneryService sceneryService;

	@Resource
	TickettypeMapper tickettypeMapper;
	/**
	 * find by id
	 *
	 * @param scId 景区id
	 * @author zjc
	 * @return R<String> map
	 */
	@GetMapping("/dateNum")
	public R<List<Integer>> getTicketDateNum(Long scId){
		 List<Integer> res;

		 res = ticketService.getMapForDateAndNum(scId);

		 if(res.isEmpty()){
			 return R.error("无数据");
		 }
		 return R.success(res);
	}
	@GetMapping("/adminlist")
	R<PageInfo<TicketListVo>> getDetailAll(HttpServletRequest request, int pageNum, int pageSize, String search, Integer value,String sDate){
		PageHelper.startPage(pageNum,pageSize);
		R<List<TicketListVo>> r = ticketService.getTicketAdminList(search,value,sDate);
		List<TicketListVo> list = r.getData();


		PageInfo<TicketListVo> pageInfo = new PageInfo<TicketListVo>(list);
		pageInfo.setTotal(ticketService.getCount(search,value,sDate));
		return R.success(pageInfo);
	}

	@PutMapping("/update")
	public R<String> updateTicket(@RequestBody TicketListVo ticketListVo){
		Long tid = ticketListVo.getTicketId();
		Ticket ticket = ticketService.getById(tid);
		if(!ticket.getNum().equals(ticketListVo.getNum())){
			ticket.setNum(ticketListVo.getNum());
			ticketService.saveOrUpdate(ticket);
		}

		Long scId = ticketListVo.getScId();
		Scenery scenery = sceneryService.getById(scId);

		LambdaUpdateWrapper<Scenery> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
		double f1 = 1.0*ticketListVo.getAdultPrice()/ticketListVo.getChildPrice();
		double f2 = 1.0*ticketListVo.getStudentPrice()/ticketListVo.getChildPrice();
		scenery.setScPrice(ticketListVo.getChildPrice());
		tickettypeMapper.updateAdultSaleByScId(scId,new BigDecimal(f1));
		tickettypeMapper.updateStudentSaleByScId(scId,new BigDecimal(f2));

		return R.success("修改成功");


	}

	@GetMapping("/delete")
	public R<String> StopSell(Long ticketId){

		Ticket ticketServiceById = ticketService.getById(ticketId);
		ticketServiceById.setTkStatus(!ticketServiceById.getTkStatus());

		ticketService.updateById(ticketServiceById);
		return R.success("操作成功");
	}

	@GetMapping("/getTkdate")
	public R<String> getTkdate(Long ticketId){

		Ticket ticket = ticketService.getById(ticketId);
		Date tkDate = ticket.getTkDate();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String d = format.format(tkDate);
		return R.success(d);
	}

}

