package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzz.common.R;
import com.gzz.common.StringIntersection;
import com.gzz.pojo.GoodsInfo;
import com.gzz.pojo.Scenery;
import com.gzz.service.GoodsInfoService;
import com.gzz.service.ImgsRespService;
import com.gzz.service.OssUploadService;
import com.gzz.vo.GoodsInfoList;
import com.gzz.vo.GoodsVo;
import com.gzz.vo.StrategyDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.gzz.utils.OssUtils.GoodTypeId;
import static com.gzz.utils.OssUtils.ViewTypeId;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/goods-info")
public class GoodsInfoController {
    @Autowired
    GoodsInfoService goodsInfoService;
    @Autowired
    ImgsRespService imgsRespService;
    @Autowired
    OssUploadService ossUploadService;
    @PostMapping("/upload")
    public R<String> Scupload(String type,@RequestParam("file") MultipartFile multipartFile){
        String imgUrl = ossUploadService.uploadFile(multipartFile,type);
        if(imgUrl!="error"){
            return R.success(imgUrl);
        }
        return R.error(imgUrl);
    }
    @GetMapping
    public R<List<GoodsInfo>> getAll(@RequestParam(name="pageSize") Integer pageSize,@RequestParam(name="pageNo")Integer pageNo)
    {
        List<GoodsInfo> goodsInfoList = goodsInfoService.list();
        if((pageNo-1)*pageSize<goodsInfoList.size())
        {
            goodsInfoList.subList((pageNo-1)*pageSize,pageNo*pageSize-1>goodsInfoList.size()-1?goodsInfoList.size()-1:pageNo*pageSize-1);
        }
        else{
            goodsInfoList.clear();
        }
        return R.success(goodsInfoList);
    }
    @GetMapping("/search")
    public R<List<GoodsInfo>> getResult(@RequestParam(name="search") String search)
    {
        List<GoodsInfo> goodsInfoList = goodsInfoService.list();
        for(GoodsInfo goodsInfo : goodsInfoList)
        {
            if(!StringIntersection.hasIntersection(goodsInfo.toString(),search))
            {
                goodsInfoList.remove(goodsInfo);
            }
        }
        return R.success(goodsInfoList);
    }
    @GetMapping("/detail")
    public R<GoodsInfo> getDetail(@RequestParam(name = "goodsId") Long goodsId)
    {
          return R.success(goodsInfoService.getById(goodsId));
    }

    @GetMapping("/moreImg")
    public R<List<String>> getMoreImg(@RequestParam(name = "goodsId") Long goodsId){
        List<String> strings = imgsRespService.getlistByIdAndTypeId(new StrategyDetailVo(),goodsId, GoodTypeId);
        return R.success(strings);
    }

    @GetMapping("/searchlist")
    public R<GoodsInfoList> getSearchList(@RequestParam(name= "pageNum")Integer pageNum, @RequestParam(name = "pageSize")Integer pageSize, @RequestParam(name="searchName")String searchName, @RequestParam(name = "searchKeyWord")String searchKeyWord, @RequestParam(name="topPrice")Double topPrice, @RequestParam(name = "bottomPrice")Double bottomPrice)
    {
        //searchName = '%'+searchName+'%';
        //searchKeyWord = '%' + searchKeyWord + '%';
        QueryWrapper<GoodsInfo> goodsInfoQueryWrapper = new QueryWrapper<>();
        goodsInfoQueryWrapper.like(searchName!="","goods_Name",searchName);
        goodsInfoQueryWrapper.like(searchKeyWord!="","goods_Desc",searchKeyWord);
        if(topPrice.toString().length()!=0&&bottomPrice.toString().length()!=0&&topPrice>bottomPrice)
        {
            goodsInfoQueryWrapper.between("goods_Price",bottomPrice,topPrice);
        }
        Page<GoodsInfo> goodsInfoPage = new Page<>(pageNum,pageSize);
        goodsInfoService.page(goodsInfoPage,goodsInfoQueryWrapper);
        List<GoodsInfo> goodsInfoList = goodsInfoPage.getRecords();
        List<GoodsVo> goodsVoList = goodsInfoService.getVo(goodsInfoList);

        GoodsInfoList goodsInfoList1 = new GoodsInfoList(goodsVoList,goodsInfoService.list().size());
        return R.success(goodsInfoList1);
    }

    @GetMapping("/reset")
    public R<GoodsInfoList> getReset(@RequestParam(name="pageNum")Integer pageNum,@RequestParam(name="pageSize")Integer pageSize)
    {
        Page<GoodsInfo> goodsInfoPage = new Page<>(pageNum,pageSize);
        goodsInfoService.page(goodsInfoPage);
        List<GoodsInfo> goodsInfoList  = goodsInfoPage.getRecords();
        List<GoodsVo> goodsVoList = goodsInfoService.getVo(goodsInfoList);
        GoodsInfoList goodsInfoList1 = new GoodsInfoList(goodsVoList,goodsInfoService.list().size());
        return R.success(goodsInfoList1);
    }
    @DeleteMapping("/delete")
    public R GoodsDelete(@RequestParam(name = "goodsId") Long goodsId)
    {


        goodsInfoService.removeByIdAndTypeId(Long.valueOf(goodsId),GoodTypeId);
        goodsInfoService.removeById(goodsId);
        return R.success(true);
    }

    @PutMapping("/update")
    public R<String> goodsUpdate(@RequestBody GoodsVo goodsVo)
    {


        GoodsInfo goodsInfo = new GoodsInfo();

        BeanUtils.copyProperties(goodsVo,goodsInfo);

        goodsInfoService.saveOrUpdate(goodsInfo);

        //存入图片表
        List<String> imgs = goodsVo.getImgs();

        Integer typeId = GoodTypeId;
        Long id = goodsVo.getGoodsId();
        imgsRespService.removeByIdAndTypeId(id,typeId);
        boolean r = imgsRespService.saveList(imgs,id,typeId);

        if(!r){
            return R.error("数据异常");
        }

        return R.success("success");
    }


    @PostMapping("/addgoods")
    public R goodsAdd(@RequestBody GoodsVo goodsVo)
    {
        goodsInfoService.saveVo(goodsVo);
        return R.success(true);
    }


}

