package com.gzz.controller;


import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzz.common.R;
import com.gzz.pojo.UserAdvise;
import com.gzz.service.ImgsRespService;
import com.gzz.service.UserAdviseService;
import com.gzz.utils.JWTUtils;
import com.gzz.utils.OssUtils;
import com.gzz.vo.AdviceList;
import com.gzz.vo.AdviseVo;
import org.aopalliance.aop.Advice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.gzz.utils.OssUtils.FeedBack;
import static com.gzz.utils.OssUtils.StrategyTypeId;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/user-advise")
public class UserAdviseController {

	@Autowired
	ImgsRespService imgsRespService;
	@Autowired
	UserAdviseService userAdviseService;

	@PostMapping("/add")
	public R<String> add(HttpServletRequest request, @RequestBody AdviseVo da){
		String token = request.getHeader("token");
		Long userId = JWTUtils.parseToken(token);

		List<String> imgs = da.getImgs();

		Long AdviseId = (long)Math.abs(UUID.randomUUID().hashCode());

		UserAdvise userAdvise = new UserAdvise();

		BeanUtils.copyProperties(da,userAdvise);

		userAdvise.setUserId(userId);

		userAdvise.setId(AdviseId);

		userAdviseService.save(userAdvise);

		Integer typeId = FeedBack;


		boolean r = imgsRespService.saveList(imgs,AdviseId,typeId);
		if(!r){
			return R.error("数据异常");
		}
		return  R.success("成功提交建议");

	}
	@GetMapping("/list")
	public R<AdviceList> getAdviceList(@RequestParam(name="pageNum")Integer pageNum, @RequestParam(name="pageSize") Integer pageSize, @RequestParam(name = "searchUserId")String searchUserId, @RequestParam(name="searchKeyWord")String searchKeyWord, @RequestParam(name="status")String status, @RequestParam(name = "attitude")String attitude)
	{
		QueryWrapper<UserAdvise> userAdviseQueryWrapper = new QueryWrapper<>();
		if(searchUserId.toString().length()!=0){
			userAdviseQueryWrapper.eq("user_id",Long.valueOf(searchUserId));
		}

		userAdviseQueryWrapper.like(searchKeyWord!="","context",searchKeyWord);
		if(attitude.length()!=0) {
			userAdviseQueryWrapper.eq("attitude", Integer.valueOf(attitude));
		}
		if(status.length()!=0){
			userAdviseQueryWrapper.eq("isRead",Integer.valueOf(status));
		}

		Page<UserAdvise> userAdvisePage = new Page<>(pageNum,pageSize);
		userAdviseService.page(userAdvisePage,userAdviseQueryWrapper);
		List<UserAdvise> userAdviseList = userAdvisePage.getRecords();
		List<AdviseVo> adviseVoList = userAdviseService.getVo(userAdviseList);
		AdviceList adviceList = new AdviceList(adviseVoList,userAdviseService.list().size());
		return R.success(adviceList);

	}

	@GetMapping("/reset")
	public R<AdviceList> getAll(@RequestParam(name="pageNum")Integer pageNum,@RequestParam(name = "pageSize")Integer pageSize)
	{
		Page<UserAdvise> userAdvisePage = new Page<>(pageNum,pageSize);
		userAdviseService.page(userAdvisePage);
		List<UserAdvise> userAdviseList = userAdvisePage.getRecords();
		List<AdviseVo> adviseVoList = userAdviseService.getVo(userAdviseList);
		AdviceList adviceList = new AdviceList(adviseVoList,userAdviseService.list().size());
		return R.success(adviceList);
	}

}

