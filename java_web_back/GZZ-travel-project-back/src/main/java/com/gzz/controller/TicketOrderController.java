package com.gzz.controller;


import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.Dto.TicketOrderDto;
import com.gzz.common.R;
import com.gzz.pojo.*;
import com.gzz.service.PlayerPersonService;
import com.gzz.service.TicketOrderService;
import com.gzz.service.TicketService;
import com.gzz.service.TikcetOrderDetailService;
import com.gzz.utils.JWTUtils;
import com.gzz.utils.RedisCache;
import com.gzz.utils.RedisKeyUtils;
import com.gzz.vo.StrategyDetailVo;
import com.gzz.vo.TicketOrderVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器，门票订单服务类
 * </p>
 *
 */
@Slf4j
@RestController
@RequestMapping("/api/ticket-order")
public class TicketOrderController {
	@Autowired
	TicketOrderService ticketOrderService;

	@Autowired
	TikcetOrderDetailService tikcetOrderDetailService;

	@Autowired
	PlayerPersonService playerPersonService;
	@Autowired
	TicketService ticketService;

	@Autowired
	RedisCache redisCache;

	@GetMapping("/update-tatus")
	public R<String> updateStatus(Long orderId,Integer status){
		TicketOrder byId = ticketOrderService.getById(orderId);
		byId.setStatus(status);
		ticketOrderService.updateById(byId);
		return R.success("修改成功");

	}

	@GetMapping("/list")
	public R<List<TicketOrder>> getAllOrders(HttpServletRequest request){
		String token = request.getHeader("token");
		Long userId = JWTUtils.parseToken(token);
		LambdaQueryWrapper<TicketOrder> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		lambdaQueryWrapper.eq(TicketOrder::getUserId,userId).orderByAsc(TicketOrder::getStatus).orderByDesc(TicketOrder::getCreateTime);

		List<TicketOrder> list = ticketOrderService.list(lambdaQueryWrapper);
		if(list!=null){
			return R.success(list);
		}
		return R.error("无法获取！！");
	}

	@GetMapping("/adminlist")
	R<PageInfo<TicketOrderVo>> getDetailAll(HttpServletRequest request, int pageNum, int pageSize, String search, Integer status, Long userId,Long orderId){
		PageHelper.startPage(pageNum,pageSize);
		R<List<TicketOrderVo>> r = ticketOrderService.getDetailAll(search,status,userId,orderId);
		List<TicketOrderVo> list = r.getData();

		PageInfo<TicketOrderVo> pageInfo = new PageInfo<>(list);
		pageInfo.setTotal(ticketOrderService.getTotalbyCondition(search,status,userId,orderId));
		return R.success(pageInfo);
	}
	@GetMapping("/detail")
	public R<TicketOrderVo> getDeailById(Long orderId){
		R<TicketOrderVo> detailById = ticketOrderService.getDetailById(orderId);
		return R.success(detailById.getData());

	}


	@GetMapping("/pay")
	public R<String> toPay(Long orderId){
		TicketOrder ticketOrder = ticketOrderService.getById(orderId);
		if(ticketOrder==null){
			return R.error("订单不存在");
		}
		if(ticketOrder.getStatus()!=0){
			return R.error("订单已经支付");
		}
		ticketOrder.setStatus(1);
		ticketOrderService.updateById(ticketOrder);
		return R.success("订单支付成功");
	}

	@PostMapping("/create")
	public R<Long> CreateTicketOrder(HttpServletRequest request, @RequestBody TicketOrderDto ticketOrderDto){

		String token = request.getHeader("token");
		Long userId = JWTUtils.parseToken(token);

		List<PlayerPerson> pLlist = ticketOrderDto.getPlayerPerson();

		for(PlayerPerson it : pLlist){
			Long plid =  (long)Math.abs(UUID.randomUUID().hashCode());
			it.setId(plid);
			it.setUserId(userId);
			playerPersonService.save(it);
		}

		Long OrderId = (long)Math.abs(UUID.randomUUID().hashCode());

		String tt = OrderId.toString().substring(0,8);

		OrderId = Long.valueOf(tt);


		TicketOrder ticketOrder = new TicketOrder();
		//1
		ticketOrder.setOrderId(OrderId);
		//2
		ticketOrder.setUserId(userId);

		BigDecimal p = ticketOrderDto.getPrice();
		Integer num = ticketOrderDto.getNum();
		//3设值数量
		ticketOrder.setNum(num);

		BigDecimal sum = p.multiply(new BigDecimal(num));
		//4.设置总价
		ticketOrder.setSumPrice(sum);
		//5.设置时间
		ticketOrder.setCreateTime(new Date());
		//6.设置景区id
		ticketOrder.setScId(ticketOrderDto.getScId());

		ticketOrder.setScName(ticketOrderDto.getScName());
		//获取票订单中的门票号
		Long scI = ticketOrderDto.getScId();

		Date d = ticketOrderDto.getDate();

		LambdaQueryWrapper<Ticket> ticketLambdaQueryWrapper = new LambdaQueryWrapper<>();

		ticketLambdaQueryWrapper.eq(Ticket::getTkDate,d).eq(Ticket::getScId,scI);

		Ticket ticket = ticketService.getOne(ticketLambdaQueryWrapper);
		//更新票数
		// 创建新的票
		if(ticket==null){
			ticket = ticketService.creatTicketByTkDateAndScId(scI,d);
		}
		Long TicketId  = ticket.getTicketId();
		ticket.setNum(ticket.getNum()-num);
		ticket.setSellNum(ticket.getSellNum()+num);
		ticketService.updateById(ticket);
		//7.获取ticketid
		ticketOrder.setTicketId(TicketId);
		//保存到数据库
		ticketOrderService.save(ticketOrder);
		//获取门票订单详情
		List<TikcetOrderDetail> list = tikcetOrderDetailService.getListByDto(ticketOrderDto,ticketOrder);

		for(TikcetOrderDetail it :list){
			tikcetOrderDetailService.save(it);
		}

		redisCache.setCacheObject(RedisKeyUtils.OrderId+tt,OrderId,10, TimeUnit.MINUTES);
		return R.success(OrderId);

	}
}

