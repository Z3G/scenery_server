package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.common.R;
import com.gzz.pojo.Notice;
import com.gzz.service.NoticeService;
import com.gzz.vo.UserDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器，公告信息管理
 * </p>
 *
 */
@RestController
@RequestMapping("/api/notice")
public class NoticeController {

	@Autowired
	NoticeService noticeService;

	@GetMapping("/adminlist")
	public R<PageInfo<Notice>> getAdminlist(int pageSize,int pageNum){
		PageHelper.startPage(pageNum,pageSize);
		List<Notice> list = noticeService.list();
		PageInfo<Notice> pageInfo = new PageInfo<>(list);
		return R.success(pageInfo);
	}
	@PutMapping("/update")
	public R<String> updateNotice(@RequestBody Notice notice){
		boolean b = noticeService.updateById(notice);
		if(b){
			return R.success("修改成功");
		}else {
			return R.error("修改失败");
		}
	}
	@PostMapping("/add")
	public R<String>addNotice(@RequestBody Notice notice){
		notice.setPublishDate(new Date());
		boolean save = noticeService.save(notice);
		if(save){
			return R.success("添加成功");
		}else{
			return R.error("添加失败");
		}

	}
	@GetMapping("/updatestatus")
	public R<String>updateStatus(Long id,Integer status){
		LambdaUpdateWrapper<Notice> lambdaUpdateWrapper= new LambdaUpdateWrapper<>();
		lambdaUpdateWrapper.eq(Notice::getId,id).set(Notice::getStatus,status);
		boolean update = noticeService.update(lambdaUpdateWrapper);
		if(update){
			return R.success("成功");
		}
		return R.error("失败");


	}

}

