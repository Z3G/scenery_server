package com.gzz.controller;


import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzz.common.R;
import com.gzz.pojo.Lost;
import com.gzz.service.LostService;
import com.gzz.service.OssUploadService;
import com.gzz.vo.LostList;
import com.gzz.vo.lostVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/lost")
public class LostController {

	// 自动装配
	@Autowired
	LostService lostService;

	// 文件上传服务类装配
	@Autowired
	OssUploadService ossUploadService;
	@PostMapping("/upload")
	public R<String> Scupload(String type,@RequestParam("file") MultipartFile multipartFile){

		// 阿里云对象存储服务器，存入图片文件，最后放回图片url地址
		String imgUrl = ossUploadService.uploadFile(multipartFile,type);
		if(imgUrl!="error"){
			return R.success(imgUrl);
		}
		return R.error(imgUrl);
	}

	// 返回给用户端失物列表
	@GetMapping("/list")
	public R<List<lostVo>> lostList(String keyword){
		// mybatisplus条件查询器
		LambdaQueryWrapper<Lost> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.like(Lost::getName,keyword).orderByDesc(Lost::getFindTime);
		List<Lost> list = lostService.list(lambdaQueryWrapper);
		List<lostVo> resList = new ArrayList<>();

		// foreach遍历
		list.forEach(it->{
			// it是list列表中一项数据，根据前端需要的数据进行封装，添加到结果列表，最后返回给前端渲染
			resList.add(new lostVo(it.getId(),it.getName(),it.getLostLocation(),
					it.getFindTime(), it.getReceiveLoction() ,it.getLostPhone(),it.getUrl()));
		});
		return R.success(resList);
	}


	// 返回给管理端的失物列表
	@GetMapping("/lostlist")
	public R<LostList> getLostList(@RequestParam(name = "pageNum")Integer pageNum,@RequestParam(name = "pageSize")Integer pageSize,@RequestParam(name="searchName") String searchName,@RequestParam(name = "searchPlace") String searchPlace)
	{
		QueryWrapper<Lost> lostQueryWrapper = new QueryWrapper<>();
		// 设置查询条件，并且只有传入的参数不为空条件才成立
		lostQueryWrapper.like(searchName!="","name",searchName);
		lostQueryWrapper.like(searchPlace!="","lost_location",searchPlace);
		// 这里使用的是mybatisplus的分页器
		Page<Lost> lostPage = new Page<>(pageNum,pageSize);
		lostService.page(lostPage,lostQueryWrapper);
		List<Lost> lostList = lostPage.getRecords();
		// 封装返回结果给前端（数据列表，数据总量）
		LostList lostList1 = new LostList(lostList,lostService.list().size());

		return R.success(lostList1);
	}


	// 重置，没有任务条件限制的，失物列表
	@GetMapping("/reset")
	public R<LostList> lostList(@RequestParam(name = "pageNum")Integer pageNum,@RequestParam(name = "pageSize")Integer pageSize)
	{
		// mybatisplus提供的分页插件，设置页码，每页大小
		Page<Lost> lostPage = new Page<>(pageNum,pageSize);


		// 执行分页查询
		lostService.page(lostPage);
		List<Lost> lostList = lostPage.getRecords();


		// 封装返回结果给前端（数据列表，数据总量）
		LostList lostList1 = new LostList(lostList,lostService.list().size());

		return R.success(lostList1);
	}
	// 根据id删除失物信息
	@DeleteMapping("/delete")
	public R lostDelete(@RequestParam(name = "id")Integer id)
	{
		lostService.removeById(id);
		return R.success(true);

	}
	// 更新失物信息
	@PutMapping("/update")
	public R lostUpdate(@RequestBody Lost lost)
	{
		// 调用mybatisplus提供的更新方法，传入一个lost对象，会解析出lost中主键id,然后根据对象中属性值进行更新数据
		lostService.updateById(lost);
		return R.success(true);
	}
	// post请求，从请求体中获取名为lost的json字符串，然后自动转换为java对象
	@PostMapping("/add")
	public R lostAdd(@RequestBody Lost lost)
	{
		// 生成id
		Long id = (long)Math.abs(UUID.randomUUID().hashCode());
		lost.setId(id);

		
		// 保存失物数据，使用的mybatisplus框架的提供的保存数据到数据库中的方法
		lostService.save(lost);
		return R.success(true);
	}

}

