package com.gzz.controller;

import com.gzz.common.R;
import com.gzz.service.HomePageService;
import com.gzz.vo.HomePageContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/home-page")
public class HomePageController {

    @Autowired
    HomePageService homePageService;
    @GetMapping
    public R<HomePageContent> getHomePage()
    {
        return R.success(homePageService.getHomeContent());
    }

}
