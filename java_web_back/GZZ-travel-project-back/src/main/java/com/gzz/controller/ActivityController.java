package com.gzz.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.common.R;
import com.gzz.pojo.Activity;
import com.gzz.pojo.Scenery;
import com.gzz.service.ActivityService;
import com.gzz.service.ImgsRespService;
import com.gzz.utils.MyPage;
import com.gzz.vo.ActivityVo;
import com.gzz.vo.SenceryDetailVo;
import com.gzz.vo.StrategyDetailVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.gzz.utils.OssUtils.ActivityTypeId;
import static com.gzz.utils.OssUtils.ViewTypeId;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Slf4j // 开启日志
@RestController // 声明这是一个controller，接受处理前端的请求
@RequestMapping("/api/activity") //统一加个前缀，就不用每个@GetMapping（）里面都写这个"/api/activity"
public class ActivityController {
    @Autowired // 自动装配，从spring容器中获取这个对象
    ActivityService activityService; // 活动服务类，服务类可以操作数据库，这里操作的是活动信息表

    // 前端需要展示图片的时候，只要把url放在图片标签就可以
    @Autowired
    ImgsRespService imgsRespService; // 图片服务类，操作图片表，数据库有一个图片表，主要存图片的url路径，


    // 获取前端需要展示的活动列表信息（用户端）
    @GetMapping("/list") // @getMapping 处理get请求，@RequestParam参数映射，get请求中url带了参数，
    // 通过这个注解可以把url路径中的参数映射到java方法的参数。（页码，每页多少行）
    public R<List<Activity>> getAll(@RequestParam(name = "curPage")Integer curPage,
                                    @RequestParam(name = "pageSize")Integer pageSize){
        // 获取数据库中活动表的所有数据，使用的是mybatisplus的框架提供的方法，
        List<Activity> activityList = activityService.list();
        // 分页展示，手写分页
        Integer num = activityList.size();
        // 最后一行数据
        Integer end = (curPage)*pageSize;
        if(num<(curPage)*pageSize){
            end = num;
        }
        // 截取列表
        activityList = activityList.subList((curPage-1)*pageSize,end);
        // 设置总页数，因为前端需要这个数据
        for (Activity activity : activityList) {
            activity.setTotNum(num);
        }
        log.info("完成查询返回数组");
        return R.success(activityList);
    }

    // 获取活动详情数据，从url路径中获取景点活动id,根据id查询详情内容
    @GetMapping("/detail")
    public R<ActivityVo> getDetail(@RequestParam(name = "scId") Long scId)
    {
        return activityService.getDetailbyId(scId);
    }

    // 返回活动详情图片，get请求
    @GetMapping("/moreImg")
    public R<List<String>> getMore(@RequestParam(name = "scId") Long scId){
        // new StrategyDetailVo()只是一个填充，只是为了复用之前的操作攻略相关图片表的方法。
        List<String> strings = imgsRespService.getlistByIdAndTypeId(new StrategyDetailVo(), scId, ActivityTypeId);
        return R.success(strings);
    }
    // 管理端景点活动列表，页码，分页大小，search是按照名称查询，如果为空则不按照名称查询，
    // value是按照活动的状态查询，有已结束，未开始，进行中这些状态
    @GetMapping("/adminlist")
    R<MyPage<ActivityVo>> getDetailAll(HttpServletRequest request, int pageNum,
                                       int pageSize, String search, Integer value){
        // 通过搜索的关键词和活动状态来获取所有的景点活动详情，
        // 如果关键词为空则不按关键词查询，如果状态为空则不按状态查询
        R<List<ActivityVo>> r = activityService.getDetailAll(search,value);
        List<ActivityVo> list = r.getData();
        // 分页，先获取所有数据的总和
        int total = list.size();
        // 当前页最后一行数据
        Integer end = (pageNum)*pageSize;
        // 当前页最开始一行的数据
        Integer start =  (pageNum-1)*pageSize;
        // 最后一行是否超过了所有数据的个数
        if(end>total){
            end = total;
        }
        // 将数据封装到Mypage对象中，mypqge中包含一个数据列表，一个数据总量。
        MyPage<ActivityVo> pageInfo = new MyPage<>();
        pageInfo.setList(list.subList(start,end));
        pageInfo.setTotal(total);
        return R.success(pageInfo);
    }
    // post请求添加新的活动信息，从请求体中获取前端传过去的activityVo数据。
    // @requestBody可以把前端传过来的对象转换为java对象
    @PostMapping("/add")
    public  R<String> addDetail(HttpServletRequest httpServletRequest,@RequestBody ActivityVo activityVo){

        Activity activity = new Activity();
        // 生成id
        Long id = (long)Math.abs(UUID.randomUUID().hashCode());
        // 两个对象中相同属性复制,使用了spring框架提供的工具类
        BeanUtils.copyProperties(activityVo,activity);
        // 设置活动id
        activity.setActId(id);
        // 往数据库保存活动信息，mybatisplus框架提供的保存方法
        activityService.save(activity);

        // 获取前端传来的活动中图片列表
        List<String> imgs = activityVo.getActImages();

        Integer typeId = ActivityTypeId;
        // 按列表存入图片表
        boolean r = imgsRespService.saveList(imgs,id,typeId);

        if(!r){
            return R.error("数据异常");
        }

        return R.success("success");
    }
    // put请求修改活动信息
    @PutMapping("/update")
    public  R<String> updateDetail(HttpServletRequest httpServletRequest,@RequestBody ActivityVo activityVo){
        // 存入活动表
        Activity activity = new Activity();
        BeanUtils.copyProperties(activityVo,activity);
        // 更新数据库，利用mybatisplus框架
        activityService.saveOrUpdate(activity);
        // 存储图片路径
        List<String> imgs = activityVo.getActImages();
        // 图片类型id,与活动id可以确定唯一图片
        Integer typeId = ActivityTypeId;
        Long id = activityVo.getActId();
        // 修改图片表之前先删除，再保存新的，因为也不好对比之前的图片与现在的图片是否一致。
        boolean f =  imgsRespService.removeByIdAndTypeId(id,typeId);
        // 保存新的
        boolean r = imgsRespService.saveList(imgs,id,typeId);
        if(!r||!f){
            return R.error("数据异常");
        }
        return R.success("success");
    }
    // delete请求，按照id删除活动 PathVariable从url路径中获取参数
    @DeleteMapping("/delete/{actId}")
    public R<String> deleteSc(@PathVariable Long actId){
        // 从数据库中活动表删除活动，用mybatisplus框架提供的方法
        activityService.removeById(actId);
        // 从数据库中图片表中删除该活动相关的图片
        imgsRespService.removeByIdAndTypeId(actId,ActivityTypeId);
        return R.success("删除成功");
    }

    // 前台首页中的需要展示的活动列表，展示三个
    @GetMapping("/index")
    public  R<List<Activity>> getIndex(){
        List<Activity> list;
        // 调用mybatisplus的提供的list方法，获取数据库中活动信息表
        list = activityService.list();
        if(list.size()>3){
            return R.success(list.subList(0,3));
        }
        return R.success(list);
    }

}