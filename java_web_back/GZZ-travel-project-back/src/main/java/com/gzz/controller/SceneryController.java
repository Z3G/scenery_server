package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.common.R;
import com.gzz.mapper.SceneryMapper;
import com.gzz.pojo.DefaultTicketNum;
import com.gzz.pojo.Scenery;
import com.gzz.service.*;
import com.gzz.vo.SceneryListVo;
import com.gzz.vo.SenceryDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static com.gzz.utils.OssUtils.StrategyTypeId;
import static com.gzz.utils.OssUtils.ViewTypeId;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 周佳成
 * @since 2023-11-20
 */
@RestController
@RequestMapping("/api/scenery")
public class SceneryController {
	@Autowired
	SceneryService sceneryService;
	@Autowired
	ImgsRespService imgsRespService;
	@Autowired
	TicketService ticketService;
	@Autowired
	OssUploadService ossUploadService;

	@Autowired
	DefaultTicketNumService defaultTicketNumService;
	@PostMapping("/upload")
	public R<String> Scupload(String type,@RequestParam("file") MultipartFile multipartFile){
		String imgUrl = ossUploadService.uploadFile(multipartFile,type);
		if(imgUrl!="error"){
			return R.success(imgUrl);
		}
		return R.error(imgUrl);
	}



	@GetMapping("/list")
	R<List<SceneryListVo>> getList(){

		LambdaQueryWrapper<Scenery> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.orderByDesc(Scenery::getOrderWight);

		List<Scenery> list = sceneryService.list(lambdaQueryWrapper);
		List<SceneryListVo> res = new ArrayList<>();
		for(Scenery scenery : list){
			SceneryListVo sceneryListVo = new SceneryListVo();
			BeanUtils.copyProperties(scenery,sceneryListVo);
			res.add(sceneryListVo);
		}
		return R.success(res);

	}
	@GetMapping("/detail")
	R<SenceryDetailVo> getDetailbyId(Long scId){

		return sceneryService.getDetailbyId(scId);


	}
	@GetMapping("/adminlist")
	R<PageInfo<SenceryDetailVo>> getDetailAll(HttpServletRequest request,int pageNum,
											  int pageSize,String search,Integer value){
		// 利用PageHelper插件实现分页
		PageHelper.startPage(pageNum,pageSize);
		R<List<SenceryDetailVo>> r = sceneryService.getDetailAll(search,value);
		List<SenceryDetailVo> list = r.getData();

		PageInfo<SenceryDetailVo> pageInfo = new PageInfo<>(list);
		pageInfo.setTotal(sceneryService.getTotalByCondition(search,value));
		return R.success(pageInfo);
	}
	@PostMapping("/add")
	public  R<String> addDetail(HttpServletRequest httpServletRequest,
								@RequestBody SenceryDetailVo senceryDetailVo){
		//存入景区表
		Scenery scenery = new Scenery();
		BeanUtils.copyProperties(senceryDetailVo,scenery);
		sceneryService.save(scenery);
		DefaultTicketNum defaultTicketNum = new DefaultTicketNum(scenery.getScId(),2000);
		defaultTicketNumService.save(defaultTicketNum);
		if(scenery.getScPrice()>0){
			ticketService.addByScenery(scenery);
		}

		//存入图片表
		List<String> imgs = senceryDetailVo.getScImages();
		Integer typeId = ViewTypeId;

		boolean r = imgsRespService.saveList(imgs,scenery.getScId(),typeId);

		if(!r){
			return R.error("数据异常");
		}

		return R.success("success");
	}

	@PutMapping("/update")
	public  R<String> updateDetail(HttpServletRequest httpServletRequest,@RequestBody SenceryDetailVo senceryDetailVo){
		//存入景区表
		Scenery scenery = new Scenery();

		BeanUtils.copyProperties(senceryDetailVo,scenery);

		sceneryService.saveOrUpdate(scenery);

		//存入图片表
		List<String> imgs = senceryDetailVo.getScImages();

		Integer typeId = ViewTypeId;
		Long id = senceryDetailVo.getScId();
		imgsRespService.removeByIdAndTypeId(id,typeId);
		boolean r = imgsRespService.saveList(imgs,scenery.getScId(),typeId);

		if(!r){
			return R.error("数据异常");
		}

		return R.success("success");
	}
	@DeleteMapping("/delete/{scId}")
	public R<String> deleteSc(@PathVariable Long scId){
		sceneryService.removeById(scId);
		imgsRespService.removeByIdAndTypeId(scId,ViewTypeId);
		ticketService.removeByscId(scId);
		return R.success("删除成功");
	}

}

