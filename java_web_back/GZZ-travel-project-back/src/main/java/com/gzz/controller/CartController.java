package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.gzz.common.R;
import com.gzz.Dto.CartReception;
import com.gzz.pojo.Cart;
import com.gzz.pojo.GoodsInfo;
import com.gzz.service.CartService;
import com.gzz.service.GoodsInfoService;
import com.gzz.utils.JWTUtils;
import com.gzz.vo.CartInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>

 */
@Slf4j
@RestController
@RequestMapping("/api/cart")
public class CartController {
    @Autowired
    CartService cartService;
    @Autowired
    GoodsInfoService goodsInfoService;
    @DeleteMapping
    public R delGoods(HttpServletRequest request, @RequestBody CartReception cartReception)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
        QueryWrapper<Cart> cartQueryWrapper = new QueryWrapper<>();
        cartQueryWrapper.eq("user_Id",id);
        cartQueryWrapper.eq("cart_Id",cartReception.getCartId());
        cartService.remove(cartQueryWrapper);
        return R.success(true);
    }
    @PutMapping
    public R selectGoods(HttpServletRequest request, @RequestBody CartReception cartReception)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
        UpdateWrapper<Cart> cartUpdateWrapper = new UpdateWrapper<>();
        cartUpdateWrapper.eq("cart_Id",cartReception.getCartId()).set("goods_Selected",!cartReception.isGoodsSelected());
        cartUpdateWrapper.eq("user_Id",id);
        cartService.update(cartUpdateWrapper);
        return R.success(true);
    }
    @PutMapping("/sub")
    public R sub(HttpServletRequest request, @RequestBody CartReception cartReception)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
        Cart cart = cartService.getById(cartReception.getCartId());
        Integer count = cart.getGoodsNum()-1;
        UpdateWrapper<Cart> cartUpdateWrapper = new UpdateWrapper<>();
        cartUpdateWrapper.eq("cart_Id",cartReception.getCartId()).set("goods_Num",count);
        cartUpdateWrapper.eq("user_Id",id);
        cartService.update(cartUpdateWrapper);
        return R.success(true);
    }
    @PutMapping("/add")
    public R add(HttpServletRequest request,@RequestBody CartReception cartReception)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
        Cart cart = cartService.getById(cartReception.getCartId());
        Integer count = cart.getGoodsNum()+1;
        UpdateWrapper<Cart> cartUpdateWrapper = new UpdateWrapper<>();
        cartUpdateWrapper.eq("cart_Id",cartReception.getCartId()).set("goods_Num",count);
        cartUpdateWrapper.eq("user_Id",id);
        cartService.update(cartUpdateWrapper);
        return R.success(true);
    }
    @GetMapping("/all")
    public R all(HttpServletRequest request,@RequestParam(name = "isAll") boolean isAll)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
       UpdateWrapper<Cart> cartUpdateWrapper = new UpdateWrapper<>();
       cartUpdateWrapper.eq("user_Id",id);
       cartUpdateWrapper.set("goods_Selected",!isAll);
       cartService.update(cartUpdateWrapper);
        return R.success(true);
    }
    @GetMapping
    public R<List<CartInfo>> getList(HttpServletRequest request)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
       QueryWrapper<Cart>cartQueryWrapper = new QueryWrapper<>();
       cartQueryWrapper.eq("user_Id",id);
       List<Cart> cartList = cartService.list(cartQueryWrapper);
       List<CartInfo> cartInfoList = new ArrayList<>();
       for(Cart cart : cartList)
       {

           CartInfo cartInfo = new CartInfo();
           cartInfo.setCartId(cart.getCartId());
           cartInfo.setGoodsInfo(goodsInfoService.getById(cart.getGoodsId()));
           cartInfo.setGoodsNum(cart.getGoodsNum());
           cartInfo.setGoodsSelected(cart.getGoodsSelected());
           cartInfo.setGoodsId(cart.getGoodsId());
           cartInfoList.add(cartInfo);

       }
       return R.success(cartInfoList);
    }
    @PutMapping("/goods-add")
    public R<Integer> goodsAdd(HttpServletRequest request, @RequestBody CartReception cartReception)
    {

        log.info("结果是什么？"+cartReception.getGoodsId().toString());
        String token = request.getHeader("token");
        Long userId = JWTUtils.parseToken(token);
        QueryWrapper<Cart> cartQueryWrapper = new QueryWrapper<>();
        cartQueryWrapper.eq("user_Id",userId);
        cartQueryWrapper.eq("goods_Id",cartReception.getGoodsId());
        List<Cart> cartList = cartService.list(cartQueryWrapper);
        if(cartList.size()==0) {
            Cart cart = new Cart();
            cart.setGoodsId(cartReception.getGoodsId());
            cart.setGoodsNum(1);
            cart.setGoodsSelected(false);
            cart.setUserId(userId);
            cartService.save(cart);
        }
        else
        {
            Integer num = cartList.get(0).getGoodsNum();
            num+=1;
            UpdateWrapper<Cart> cartUpdateWrapper = new UpdateWrapper<>();
            cartUpdateWrapper.eq("user_Id",userId);
            cartUpdateWrapper.eq("goods_Id",cartReception.getGoodsId());
            cartUpdateWrapper.set("goods_Num",num);
            cartService.update(cartUpdateWrapper);

        }
        List<Cart>cartList1 = cartService.list();
        return R.success(cartList1.size());
    }
    @GetMapping("/num")
    R<Integer> getNumOfCart(HttpServletRequest request){
        String token = request.getHeader("token");
        Long userId = JWTUtils.parseToken(token);
        QueryWrapper<Cart> cartQueryWrapper = new QueryWrapper<>();
        cartQueryWrapper.eq("user_Id",userId);
        Integer len = cartService.list(cartQueryWrapper).size();
        return R.success(len);
    }

}

