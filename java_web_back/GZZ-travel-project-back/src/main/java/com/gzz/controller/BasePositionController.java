package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gzz.common.R;
import com.gzz.pojo.BasePosition;
import com.gzz.service.BasePositionService;
import com.gzz.utils.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@Slf4j
@RequestMapping("/api/base-position")
public class BasePositionController {

    @Autowired
    BasePositionService basePositionService;
    @PutMapping
    public R addPosition(HttpServletRequest request,@RequestBody BasePosition basePosition)
    {
        log.info("内容为"+basePosition.toString());
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
        basePosition.setUserId(id);
        basePositionService.saveOrUpdate(basePosition);
        return R.success(true);
    }

    @GetMapping("/list")
    public R getPosition(HttpServletRequest request)
    {
        String token = request.getHeader("token");
        Long id = JWTUtils.parseToken(token);
        QueryWrapper<BasePosition> basePositionQueryWrapper = new QueryWrapper<>();
        basePositionQueryWrapper.eq("user_Id",id);
        List<BasePosition> basePositionList = basePositionService.list(basePositionQueryWrapper);
        return R.success(basePositionList);
    }
}

