package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gzz.common.R;
import com.gzz.pojo.UserLikes;
import com.gzz.service.RedisService;
import com.gzz.service.UserLikesService;
import com.gzz.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 点赞记录表 前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/user-likes")
public class UserLikesController {
	@Autowired
	RedisService redisService;

	@Autowired
	UserLikesService userLikesService;

	@GetMapping("/updateLike")
	public R<String> updateLike(HttpServletRequest request, HttpServletResponse response,Long stId,Boolean status){
		String token = request.getHeader("token");
		//获取用户id
		Long id = JWTUtils.parseToken(token);
		if(status){
			redisService.saveLiked2Redis(stId,id);
		}else{
			redisService.unlikeFromRedis(stId,id);
		}
		return  R.success("更新");
	}

	@GetMapping("/getLike")
	public R<String> getLike(HttpServletRequest request,Long stId){
		String token = request.getHeader("token");
		//获取用户id
		Long id = JWTUtils.parseToken(token);
		//先从缓存里取
		Integer status = redisService.getLikeStatus(stId,id);

		if(status!=null){
			if(status==1){
				return R.success("true");
			}else{
				return R.success("false");
			}
		}

		UserLikes userLikes;

		LambdaQueryWrapper<UserLikes> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(UserLikes::getUserId,id).eq(UserLikes::getStId,stId);

		userLikes = userLikesService.getOne(lambdaQueryWrapper);

		if(userLikes==null||userLikes.getStatus()==0){
			return R.success("false");
		}
		if(userLikes.getStatus() == 1){
			return R.success("true");
		}
		return R.error("数据异常");
	}

}

