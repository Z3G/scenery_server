package com.gzz.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.common.R;
import com.gzz.pojo.Route;
import com.gzz.pojo.Scenery;
import com.gzz.service.ImgsRespService;
import com.gzz.service.RouteService;
import com.gzz.vo.RouteVo;
import com.gzz.vo.SenceryDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

import static com.gzz.utils.OssUtils.RouteTypeId;
import static com.gzz.utils.OssUtils.ViewTypeId;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController  // 声明这是一个controller，接受处理前端的请求
@RequestMapping("/api/route") //统一加个前缀，就不用每个@GetMapping（）里面都写这个"/api/route"
public class RouteController {

	@Autowired  // 自动装配 从spring容器中获取这个对象
	RouteService routeService;

	@Autowired
	ImgsRespService imgsRespService;

	// 获取管理端路线列表，分页查询，并且根据search进行模糊查询，如果search为空则不进行条件查询
	@GetMapping("/adminlist")
	R<PageInfo<RouteVo>> getDetailAll(HttpServletRequest request, int pageNum, int pageSize, String search){
		// pageHelper插件进行分页查询，先设置页码和页大小，之前的activityController中手写的分页
		PageHelper.startPage(pageNum,pageSize);
		R<List<RouteVo>> r = routeService.getDetailAll(search);
		List<RouteVo> list = r.getData();
		// 设置分页的数据列表
		PageInfo<RouteVo> pageInfo = new PageInfo<>(list);
		// 设置所有数据的个数
		pageInfo.setTotal(list.size());
		return R.success(pageInfo);
	}
	// 获取客户端需要展示的路线列表
	@GetMapping("/khlist")
	public R<List<RouteVo>> getDetailKuAll(HttpServletRequest request){
		// 传入null代表是没有条件查询，获取数据库中路线表的全部数据
		R<List<RouteVo>> r = routeService.getDetailAll(null);
		List<RouteVo> list = r.getData();

		return R.success(list);
	}
	// 获取路线信息列表详情
	@GetMapping("/detail")
	public R<RouteVo> getDetailKuAll(HttpServletRequest request,Long routeId){
		// 根据传入的id获取路线详细信息
		R<RouteVo> detailById = routeService.getDetailById(routeId);

		return R.success(detailById.getData());
	}
	// post请求添加新路线信息
	@PostMapping("/add")
	public  R<String> addDetail(HttpServletRequest httpServletRequest,@RequestBody RouteVo routeVo){
		Route route = new Route();
		// 属性复制
		BeanUtils.copyProperties(routeVo,route);
		Long id = (long)Math.abs(UUID.randomUUID().hashCode());
		route.setRouteId(id);
		// 调用mybatisplus提供的save方法新增数据到数据库中
		routeService.save(route);

		//存入图片表
		List<String> imgs = routeVo.getRouteImags();

		Integer typeId = RouteTypeId;
		boolean r = imgsRespService.saveList(imgs,id,typeId);//判断数据存储有没有成功

		if(!r){
			return R.error("数据异常");
		}

		return R.success("success");
	}
	@PutMapping("/update")
	public  R<String> updateDetail(HttpServletRequest httpServletRequest,@RequestBody RouteVo routeVo){
		Route route = new Route();

		BeanUtils.copyProperties(routeVo,route);
		// 调用mybatisplus提供的saveorUpdate方法新增或修改数据到数据库中
		routeService.saveOrUpdate(route);

		//存入图片表,路线id和类型id确认这个路线的相关图片
		List<String> imgs = routeVo.getRouteImags();

		Integer typeId = RouteTypeId;
		Long id = routeVo.getRouteId();
	   // 修改图片表之前先删除，再保存新的，因为也不好对比之前的图片与现在的图片是否一致。
		imgsRespService.removeByIdAndTypeId(id,typeId);
		boolean r = imgsRespService.saveList(imgs,id,typeId);

		if(!r){
			return R.error("数据异常");
		}

		return R.success("success");
	}
	// delete请求，根据id删除路线信息
	@DeleteMapping("/delete/{routeId}")
	public R<String> deleteSc(@PathVariable Long routeId){
		routeService.removeById(routeId);
		// 然后删除路线相关的图片
		imgsRespService.removeByIdAndTypeId(routeId,RouteTypeId);
		return R.success("删除成功");
	}




}

