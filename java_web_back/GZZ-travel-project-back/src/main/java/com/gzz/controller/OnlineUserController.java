package com.gzz.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzz.common.R;
import com.gzz.pojo.OnlineUser;
import com.gzz.service.OnlineUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/online-user")
public class OnlineUserController {
	@Autowired
	OnlineUserService onlineUserService;
	/**
	 * @author 贺嘉琦
	 * 管理端获取用户登录信息的列表，用户登录状态，什么时候登录，设备，ip,浏览器，并且分页
	 */
	@GetMapping("/find")
	R<PageInfo<OnlineUser>> findOnline( int pageNum, int pageSize, Long userId){
		// 设置分页
		PageHelper.startPage(pageNum,pageSize);
		// mybatisplus查询器
		LambdaQueryWrapper<OnlineUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		// 设置查询条件，前提是userId不等于null
		lambdaQueryWrapper.eq(userId!=null,OnlineUser::getUserId,userId);
		List<OnlineUser> list = onlineUserService.list(lambdaQueryWrapper);
		// 传入分页的后得到数据列表
		PageInfo<OnlineUser> pageInfo = new PageInfo<>(list);
		return R.success(pageInfo);
	}

}

