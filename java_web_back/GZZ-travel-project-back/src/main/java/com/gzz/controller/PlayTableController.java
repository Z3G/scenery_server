package com.gzz.controller;


import com.gzz.common.R;
import com.gzz.pojo.PlayTable;
import com.gzz.service.PlayTableService;
import com.gzz.vo.ProvinceNumVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/api/play-table")
public class PlayTableController {
	@Autowired
	PlayTableService playTableService;

	@GetMapping("/getProvince")
	public R<List<ProvinceNumVo>> getProvince(){
		return R.success(playTableService.getProvinceNumVo());
	}

	@GetMapping("/getAgePie")
	public R<List<ProvinceNumVo>> getAgePie(){

		List<PlayTable> list = playTableService.list();
		int t0 = 0;
		int t1=0;
		int t2=0;
		int t3=0;
		int t4=0;

		for(PlayTable item : list){
			String idCard = item.getIdCard();
			Integer t = Integer.valueOf(idCard.substring(6,10));
			Calendar calendar = Calendar.getInstance();
			Integer ny = calendar.get(Calendar.YEAR);
			if(ny-t<18){
				t0++;
			}
			else if(ny-t<31){
				t1++;
			}
			else if(ny-t<45){
				t2++;
			}
			else if(ny-t<61){
				t3++;
			}
			else if(ny-t>=61){
				t4++;
			}

		}
		List<ProvinceNumVo> list1 = new ArrayList<>();
		ProvinceNumVo provinceNumVo = new ProvinceNumVo("5-17",t0);
		list1.add(provinceNumVo);
		ProvinceNumVo provinceNumVo1 = new ProvinceNumVo("18-30",t1);

		ProvinceNumVo provinceNumVo2 = new ProvinceNumVo("31-44",t2);

		ProvinceNumVo provinceNumVo3 = new ProvinceNumVo("45-60",t3);

		ProvinceNumVo provinceNumVo4 = new ProvinceNumVo("60以上",t4);

		list1.add(provinceNumVo1);
		list1.add(provinceNumVo2);
		list1.add(provinceNumVo3);
		list1.add(provinceNumVo4);

		return R.success(list1);
	}
}

