package com.gzz.ScheduledTask;

import com.alibaba.fastjson.JSON;
import com.gzz.mapper.PlayTableMapper;
import com.gzz.pojo.PlayTable;
import com.gzz.service.PlayTableService;
import com.gzz.service.impl.WebSocket;
import com.gzz.utils.IDCardsUitls;
import com.gzz.utils.TimeUtils;
import com.gzz.vo.plyerNumby3MinVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

@Slf4j
@Component
public class pushPlayerNumBy3Min {
	@Resource
	PlayTableMapper playTableMapper;
	@Scheduled(cron = "0 0/1 * * * *")
	public void pushMessage() throws IOException {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("HH:mm");

		String d = format.format(date);
		// 模拟获取实时数据
		Integer num = playTableMapper.getNumBy3min();
		num = Math.abs(UUID.randomUUID().hashCode())%1000;
		Integer todayNum = playTableMapper.
				getTodayNum(TimeUtils.getToday0PointDatetime(new Date()),
						TimeUtils.getTomorrow0PointDatetime(new Date()));
		todayNum =  Math.abs(UUID.randomUUID().hashCode())%10000;
		plyerNumby3MinVo p = new plyerNumby3MinVo(d,num,todayNum);
		WebSocket.sendMessage(JSON.toJSONString(p),"1");
	}



}
