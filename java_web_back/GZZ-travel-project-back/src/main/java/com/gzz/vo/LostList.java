package com.gzz.vo;

import com.gzz.pojo.Lost;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class LostList {
   private List<Lost> lostList;
   private Integer total;
}
