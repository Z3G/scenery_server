package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
// 门票列表项vo
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketListVo implements Serializable {

	private Long ticketId;

	private Long scId;

	private Integer num;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh", timezone = "GMT+8")
	private Date tkDate;

	private Boolean tkStatus;

	private Integer initNum;

	private Integer sellNum;

	private Integer childPrice;

	private Integer studentPrice;

	private Integer adultPrice;

	private String scName;
}
