package com.gzz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StrategyListVo implements Serializable {
	private Long stId;
	private String stTitle;
	private String stCoverImage;
	private Integer stZan;
	private Integer stSc;
	private Integer stComment;
}
