package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsVo {

    private Long goodsId;

    private String goodsPic;

    private String goodsName;

    private String goodsDesc;

    private Integer type;

    private BigDecimal goodsPrice;

    private BigDecimal goodsOriginPrice;

    List<String> imgs;
}
