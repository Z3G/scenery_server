package com.gzz.vo;

import com.gzz.pojo.GoodsInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsInfoList {
    private List<GoodsVo> goodsInfoList;
    private Integer total;
}
