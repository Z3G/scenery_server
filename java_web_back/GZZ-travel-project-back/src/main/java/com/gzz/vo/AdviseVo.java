package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdviseVo implements Serializable {
	private List<String> imgs;
	private String context;
	private Integer attitude;
	private Long id;
	private Long userId;
	private Integer isRead;
	private String title;
	private String backContext;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh", timezone = "GMT+8")
	private Date createTime;
}
