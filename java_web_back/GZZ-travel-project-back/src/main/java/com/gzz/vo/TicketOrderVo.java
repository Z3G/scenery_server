package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
// 门票订单vo
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketOrderVo implements Serializable {
	@TableId(value = "order_id")
	private Long orderId;

	private Long ticketId;

	private Long userId;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh", timezone = "GMT+8")
	private Date createTime;

	private Integer status;

	private BigDecimal sumPrice;

	private Integer num;

	private Long scId;

	private String scName;

	private List<TicketOrderDetailVo> orderDetail;


}
