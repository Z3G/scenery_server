package com.gzz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
/**
 * @author zjc12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProvinceNumVo implements Serializable {
	private String name;
	private Integer value;
}
