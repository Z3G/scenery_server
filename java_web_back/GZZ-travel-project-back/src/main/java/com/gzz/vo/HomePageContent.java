package com.gzz.vo;

import com.gzz.pojo.Scenery;
import com.gzz.pojo.Strategy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomePageContent {
    private List<Strategy> strategiesList;
    private List<Scenery> sceneryList;

}
