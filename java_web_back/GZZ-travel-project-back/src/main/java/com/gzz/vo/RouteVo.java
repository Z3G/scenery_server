package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RouteVo implements Serializable {
	private static final long serialVersionUID=1L;

	@TableId(value = "route_id", type = IdType.AUTO)
	private Long routeId;

	private Integer routeDay;

	private String routeName;

	private String routeCoverImg;

	private String routeSeason;

	private String routeContent;

	private List<String>  routeImags;
}
