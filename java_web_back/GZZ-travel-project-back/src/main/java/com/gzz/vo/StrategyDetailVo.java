package com.gzz.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * @author zjc12
 */
@Data
public class StrategyDetailVo implements Serializable {
	private Long stId;
	private Integer stZan;
	private Integer stSc;
	private Integer stComment;
	private String stTitle;
	private String stCoverImage;
	private String context;
	private List<String> detailImgs;
	private boolean pub;
	private Long userId;
	private Integer status;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm",locale = "zh", timezone = "GMT+8")
	private Date createTime;
	public boolean getPub(){
		return  pub;
	}
}
