package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gzz.pojo.PicUrl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
/**
 * @author zjc12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodVo {
	private Long foodId;

	private String foodCoverImg;

	private String foodName;

	private String foodAddress;
	private String foodTips;

	private String foodTime;

	private String foodPhone;

	private String foodContent;

	private Long visits;

	private Integer score;

	private BigDecimal distant;

	private Integer totNum;

	private List<String> imgs;
}

