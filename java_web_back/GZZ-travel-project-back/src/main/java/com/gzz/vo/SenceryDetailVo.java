package com.gzz.vo;

import com.gzz.pojo.Tickettype;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SenceryDetailVo implements Serializable {
	private Long scId;

	private String scImage;

	private String scName;

	private String scDesc;

	private String scAddress;

	private String scTime;

	private String scPhone;

	private String scDatail;

	private Integer scPrice;

	private Boolean scStatus;

	private List<String> scImages;

	private List<Tickettype> ticketTypes;
}
