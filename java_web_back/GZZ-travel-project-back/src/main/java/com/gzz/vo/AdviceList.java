package com.gzz.vo;

import com.gzz.pojo.UserAdvise;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdviceList {

    private List<AdviseVo> userAdviseList;
    private Integer total;
}
