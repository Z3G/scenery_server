package com.gzz.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zjc12
 */
@Data
public class StrategyVo implements Serializable {

	private String stTitle;
	private String stCoverImage;
	private String context;
	private List<String> detailImgs;
}
