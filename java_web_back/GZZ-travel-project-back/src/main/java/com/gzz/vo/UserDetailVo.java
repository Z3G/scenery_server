package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailVo implements Serializable {

	private Long userId;

	private String username;

	private Integer sex;


	private String phone;

	private Boolean status;

	private String userArea;

	private Integer orderNum;

	private Integer sumPrice;

	private Integer strategyNum;

	private String userAvatar;
}
