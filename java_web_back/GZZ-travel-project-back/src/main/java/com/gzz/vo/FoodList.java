package com.gzz.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.List;

/**
 * @author zjc12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodList implements Serializable {
	private List<FoodVo> foodList;
	private Integer total;
}
