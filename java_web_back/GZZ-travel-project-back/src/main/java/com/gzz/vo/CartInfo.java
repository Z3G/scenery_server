package com.gzz.vo;

import com.gzz.pojo.GoodsInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CartInfo {
    private Long userId;
    private Long cartId;
    private Long goodsId;
    private Boolean goodsSelected;
    private Integer goodsNum;
    private GoodsInfo goodsInfo;

}
