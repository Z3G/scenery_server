package com.gzz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SceneryListVo implements Serializable {
	private Long scId;

	private String scImage;

	private String scName;

	private String scDesc;

	private Integer scPrice;

	private String scTime;
}
