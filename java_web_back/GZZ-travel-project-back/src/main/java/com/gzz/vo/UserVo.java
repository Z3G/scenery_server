package com.gzz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVo implements Serializable {
	private static final long serialVersionUID=1L;
	private Long userId;

	private String username;

	private Integer sex;

	private String userAvatar;
}
