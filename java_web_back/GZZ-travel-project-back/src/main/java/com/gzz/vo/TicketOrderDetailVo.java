package com.gzz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
// 门票订详情vo
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketOrderDetailVo implements Serializable {
	private Long orderDetailId;

	private  String bookName;

	private String palyerName;

	private  String typeName;

	private Integer price;
}
