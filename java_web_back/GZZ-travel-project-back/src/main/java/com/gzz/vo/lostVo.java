package com.gzz.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class lostVo implements Serializable {

	private Long id;

	/**
	 * 失物名称
	 */
	private String name;

	/**
	 * 失物捡取地点
	 */
	private String lostLocation;

	/**
	 * 时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm",locale = "zh", timezone = "GMT+8")
	private Date findTime;

	/**
	 * 待领取地点
	 */
	private String receiveLoction;

	/**
	 * 服务电话
	 */
	private String lostPhone;

	/**
	 * 失物图片URL
	 */
	private String url;

	public lostVo(Long id,String name,String lostLocation,Date findTime,String receiveLoction,String lostPhone, String url)
	{
		this.id = id;
		this.name = name;
		this.lostLocation = lostLocation;
		this.findTime = findTime;
		this.receiveLoction = receiveLoction;
		this.lostPhone = lostPhone;
		this.url = url;
	}


}
