package com.gzz.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivityVo implements Serializable {
	private static final long serialVersionUID=1L;

	private Long actId;

	private String actName;

	private String actTime;

	private String actAddress;
	// 活动封面
	private String actImage;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh", timezone = "GMT+8")
	private Date actDatetime;
	// 活动内容详情
	private String actDetail;

	private String actPhone;

	private Integer totNum;
    // 活动图片列表
	private List<String> actImages;
}
