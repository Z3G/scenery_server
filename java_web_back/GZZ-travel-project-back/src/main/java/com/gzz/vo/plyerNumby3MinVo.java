package com.gzz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class plyerNumby3MinVo implements Serializable {
	private String ptime;
	private Integer num;
	private Integer TodayNum;
}
