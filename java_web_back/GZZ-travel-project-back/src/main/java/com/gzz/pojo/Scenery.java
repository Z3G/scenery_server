package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 周佳成
 * @since 2023-11-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Scenery implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "sc_id")
    private Long scId;
    private String scImage;
    private String scName;
    private String scDesc;
    private String scAddress;
    private String scTime;
    private String scPhone;
    private String scDatail;
    private Integer scPrice;
    private Boolean scStatus;
    private Integer orderWight;


}
