package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Cart implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("user_Id")
    private Long userId;

    @TableId(value = "cart_Id", type = IdType.AUTO)
    private Long cartId;

    @TableField("goods_Id")
    private Long goodsId;

    @TableField("goods_Selected")
    private Boolean goodsSelected;

    @TableField("goods_Num")
    private Integer goodsNum;


}
