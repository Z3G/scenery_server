package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 门票数据模型
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Ticket implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "ticket_id", type = IdType.AUTO)
    private Long ticketId;

    private Long scId;

    private Integer num;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",locale = "zh", timezone = "GMT+8")
    private Date tkDate;

    private Boolean tkStatus;

    private Integer initNum;

    private Integer sellNum;

    private String scName;


}
