package com.gzz.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *      美食数据模型
 *
 *
 *
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Food implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "food_Id")
    private Long foodId;

    @TableField("food_Cover_Img")
    private String foodCoverImg;

    @TableField("food_Name")
    private String foodName;

    private String foodAddress;

    @TableField("food_Tips")
    private String foodTips;

    @TableField("food_Time")
    private String foodTime;

    @TableField("food_Phone")
    private String foodPhone;

    @TableField("food_Content")
    private String foodContent;

    private Long visits;

    private Integer score;

    private BigDecimal distant;

    @TableField("tot_Num")
    private Integer totNum;


}
