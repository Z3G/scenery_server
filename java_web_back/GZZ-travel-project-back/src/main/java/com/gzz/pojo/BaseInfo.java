package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
商品订单实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BaseInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("user_Id")
    private Long userId;

    @TableField("goods_Name")
    private String goodsName;

    @TableField("order_Time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",locale = "zh", timezone = "GMT+8")
    private Date orderTime;

    @TableField("goods_Num")
    private Long goodsNum;

    @TableField("sum_Price")
    private Double sumPrice;

    private Integer tab;

    @TableId(value = "order_Id", type = IdType.AUTO)
    private String orderId;


}
