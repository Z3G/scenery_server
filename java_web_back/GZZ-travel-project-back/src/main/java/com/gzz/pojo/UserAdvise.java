package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserAdvise implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 意见表id
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    private String context;

    /**
     * 态度编号，0 差评 1中立 2好评
     */
    private Integer attitude;

    /**
     * 反馈是否被回，受理 0为受理回答，1已经
     */
    @TableField("isRead")
    private Integer isRead;

    private String backContext;


}
