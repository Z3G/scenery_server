package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PlayerPerson implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id")
    private Long Id;

    private Long userId;

    @TableField("NAME")
    private String name;

    private String idCard;

    private String phone;


}
