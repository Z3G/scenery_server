package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 点赞记录表
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserLikes implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 点赞信息ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 点赞对象id
     */
    private Long stId;

    /**
     * 点赞人ID
     */
    private Long userId;

    /**
     * 0 点赞 1 取消 
     */
    private Integer status;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd :HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd : HH:mm:ss",locale = "zh", timezone = "GMT+8")
    private Date createTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd :HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd : HH:mm:ss",locale = "zh", timezone = "GMT+8")
    private Date updateTime;


}
