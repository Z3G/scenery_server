package com.gzz.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GoodsInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "goods_Id", type = IdType.AUTO)
    private Long goodsId;

    @TableField("goods_Pic")
    private String goodsPic;

    @TableField("goods_Name")
    private String goodsName;

    @TableField("goods_Desc")
    private String goodsDesc;

    private Integer type;

    @TableField("goods_Price")
    private BigDecimal goodsPrice;

    @TableField("goods_Origin_Price")
    private BigDecimal goodsOriginPrice;


}
