package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Route implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "route_id", type = IdType.AUTO)
    private Long routeId;

    private Integer routeDay;

    private String routeName;

    private String routeCoverImg;

    private String routeSeason;

    private String routeContent;


}
