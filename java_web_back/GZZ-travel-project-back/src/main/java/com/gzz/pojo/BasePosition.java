package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *  收货地址实体类
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BasePosition implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("user_Id")
    private Long userId;

    @TableId(value = "addr_Id", type = IdType.AUTO)
    private Long addrId;

    @TableField("addr_Contact")
    private String addrContact;

    @TableField("user_Sex")
    private String userSex;

    @TableField("addr_Phone")
    private String addrPhone;

    @TableField("addr_Content")
    private String addrContent;


}
