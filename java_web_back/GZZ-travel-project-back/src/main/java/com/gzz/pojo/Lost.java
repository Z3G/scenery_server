package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Lost implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 失物名称
     */
    private String name;

    /**
     * 状态 0未被收其 1被收取
     */
    private Integer status;

    /**
     * 失物捡取地点
     */
    private String lostLocation;

    /**
     * 时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",locale = "zh", timezone = "GMT+8")
    private Date findTime;

    /**
     * 待领取地点
     */
    private String receiveLoction;

    /**
     * 服务电话
     */
    private String lostPhone;

    /**
     * 失物图片URL
     */
    private String url;

    /**
     * 被领取时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd :HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd : HH:mm:ss",locale = "zh", timezone = "GMT+8")
    private Date receiveTime;

    /**
     * 领取人姓名
     */
    private String receiveName;

    /**
     * 领取人身份证号
     */
    @TableField("receive_idCard")
    private String receiveIdcard;


}
