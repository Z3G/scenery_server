package com.gzz.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *      门票类型数据模型
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Tickettype implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "type_id")
    private Integer typeId;

    private Long scId;

    private BigDecimal sale;

    private String typeName;

}
