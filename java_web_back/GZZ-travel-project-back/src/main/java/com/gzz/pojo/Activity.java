package com.gzz.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Activity implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "act_Id", type = IdType.AUTO)
    private Long actId;
    @TableField("act_Name")
    private String actName;
    @TableField("act_Time")
    private String actTime;
    @TableField("act_Address")
    private String actAddress;
    @TableField("act_Image")
    private String actImage;
    @TableField("act_Datetime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",locale = "zh", timezone = "GMT+8")
    private Date actDatetime;
    @TableField("act_Detail")
    private String actDetail;
    @TableField("act_Phone")
    private String actPhone;
    @TableField("tot_Num")
    private Integer totNum;

}
