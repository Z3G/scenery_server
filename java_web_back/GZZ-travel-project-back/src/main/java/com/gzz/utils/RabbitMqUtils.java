package com.gzz.utils;
// 工具类
public class RabbitMqUtils {
	public static String order_exchange = "order.direct";
	public static String order_routing_key = "order";

	public static String order_dlx_queue = "dlx.queue";

	public static String order_expire = "600000";
}
