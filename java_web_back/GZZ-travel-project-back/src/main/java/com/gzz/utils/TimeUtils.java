package com.gzz.utils;

import javafx.fxml.LoadException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;
//
public class TimeUtils {
	public static int  compare(String actDatetime, String actTime) {
		// 日期格式转换
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		// 当前日期（年月日）拼接 （时分）
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime d1 = LocalDateTime.parse(actDatetime+" "+actTime, formatter);
		// 进行时间比较
		if (now.isAfter(d1)) {
			return 1;
		} else if (now.isBefore(d1)) {
			return -1;
		} else {
			return 0;
		}
	}
	public static Date LocalDateToDate(LocalDate localDate){
		Date convertedDate = Date.from(localDate.atStartOfDay(ZoneId.of("Asia/Shanghai")).toInstant());
		return convertedDate;
	}
	public static LocalDate DateToLocalDate(Date date){
		return date.toInstant().atZone(ZoneId.of("Asia/Shanghai")).toLocalDate();
	}
	public static LocalDateTime DateToLocalDatetime(Date date){
		return date.toInstant().atZone(ZoneId.of("Asia/Shanghai")).toLocalDateTime();
	}
	public static  String getToday0PointString(Date date){
		LocalDate date1 = DateToLocalDate(date);
		Date date2 = LocalDateToDate(date1);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date2);
	}
	public static Date getTomorrow0PointDatetime(Date date){
			LocalDate date1 = DateToLocalDate(date);
			date1 = date1.plusDays(1L);
			Date date2 = LocalDateToDate(date1);
		    return  date2;
	}
	public static  Date getToday0PointDatetime(Date date){
		LocalDate date1 = DateToLocalDate(date);
		Date date2 = LocalDateToDate(date1);
		return date2;
	}
	public static Date getRomdDateTime(){
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		LocalDateTime localDateTime = DateToLocalDatetime(date);
		localDateTime = localDateTime.plusDays((long)Math.abs(UUID.randomUUID().hashCode())%100);
		localDateTime = localDateTime.minusDays((long)Math.abs(UUID.randomUUID().hashCode())%110);
		localDateTime = localDateTime.minusHours((long)Math.abs(UUID.randomUUID().hashCode())%8+5);
		localDateTime=localDateTime.plusMinutes((long)Math.abs(UUID.randomUUID().hashCode())%60);
		date = Date.from(localDateTime.atZone(ZoneId.of("Asia/Shanghai")).toInstant());
		return date;

	}
}
