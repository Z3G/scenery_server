package com.gzz.utils;

import lombok.Data;

import java.util.List;
@Data // 自动添加get,set方法
public class MyPage<T> {
	private List<T> list;
	private Integer total;
}
