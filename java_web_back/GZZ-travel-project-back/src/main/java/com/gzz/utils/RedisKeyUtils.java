package com.gzz.utils;
// 工具类
public class RedisKeyUtils {
	/**
	 *
	 保存用户点赞内容数据的key
	 *
	 */
	public static final String MAP_KEY_USER_LIKED = "MAP_USER_LIKED";
	/**
	 *
	 保存内容被点赞数量的key
	 *
	 */
	public static final String MAP_KEY_USER_LIKED_COUNT = "MAP_USER_LIKED_COUNT";


	//
	public static  final String MAP_KEY_USER_INFO = "MAP_KEY_USER_INFO";

	/**
	 * 拼接被点赞的内容id和点赞的人的id作为key。格式 222222::333333
	 * @param infoId 被点赞的内容 id
	 * @param likeUserId 点赞的人的id
	 * @return
	 */
	public static String getLikedKey(Long infoId, Long likeUserId){
		return infoId.toString() +
				":" +
				likeUserId.toString();
	}
	public static String OrderId = "ORDERID";
	public static String userScId = "userScId";
}
