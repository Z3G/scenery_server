package com.gzz.utils;

import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;
import cn.hutool.crypto.digest.MD5;

import javax.sound.sampled.Line;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;

public class test1 {
	public static void main(String[] args) {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String d = format.format(date);

		System.out.println(d);
		format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		LocalDate localDate = date.toInstant().atZone(ZoneId.of("Asia/Shanghai")).toLocalDate();
		System.out.println("java.util.Date转换为java.time.LocalDate: " + localDate);
		localDate  =  localDate.plusDays(1L);
		System.out.println(format.format(TimeUtils.LocalDateToDate(localDate)));
		// 将java.time.LocalDate转换为java.util.Date
		LocalDate currentDate = LocalDate.now();
		Date convertedDate = Date.from(currentDate.atStartOfDay(ZoneId.of("Asia/Shanghai")).toInstant());
		System.out.println("java.time.LocalDate转换为java.util.Date: " + convertedDate);

		System.out.println(format.format(convertedDate));
	}



	public static int  compare(String actDatetime, String actTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime d1 = LocalDateTime.parse(actDatetime+" "+actTime, formatter);

		if (now.isAfter(d1)) {
			return 1;
		} else if (now.isBefore(d1)) {
			return -1;
		} else {
			return 0;
		}
	}
}
