package com.gzz.utils;
import com.aliyun.imageaudit20191230.Client;
import com.aliyun.tea.TeaException;
import com.aliyun.imageaudit20191230.models.ScanImageRequest;
import com.aliyun.teautil.models.RuntimeOptions;
import  com.aliyun.imageaudit20191230.models.ScanImageRequest.ScanImageRequestTask;
// 工具类
public class AliContextCheck {


	public static Client createClient() throws Exception {
		com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
				.setAccessKeyId(OssUtils.accessKeyId)
				.setAccessKeySecret(OssUtils.accessKeySecret);
		config.endpoint = "imageaudit.cn-shanghai.aliyuncs.com";
		return new Client(config);
	}

	public static boolean checkImag(String url) throws Exception {
		Client client = createClient();
		String suggestion = "";
		ScanImageRequestTask task = new ScanImageRequestTask().setImageURL(url);
		ScanImageRequest scanImageRequest = new ScanImageRequest().setTask(java.util.Arrays.asList(
				task
		)).setScene(java.util.Arrays.asList(
				"porn",
				"terrorism"
		));
		RuntimeOptions runtime = new RuntimeOptions();
		try {

			suggestion = client.scanImageWithOptions(scanImageRequest, runtime).
					getBody().getData().results.get(0).
					getSubResults().get(0).suggestion;

			System.out.println(suggestion);

		} catch (TeaException error) {
			// 错误 message
			System.out.println(error.getMessage());
			// 诊断地址
			System.out.println(error.getData().get("Recommend"));
			com.aliyun.teautil.Common.assertAsString(error.message);
		} catch (Exception _error) {
			TeaException error = new TeaException(_error.getMessage(), _error);
			// 错误 message
			System.out.println(error.getMessage());
			// 诊断地址
			System.out.println(error.getData().get("Recommend"));
			com.aliyun.teautil.Common.assertAsString(error.message);
		}
		if(suggestion.equals("pass")){
			return true;
		}else{
			return false;
		}
	}

	public static boolean checkText(String text) throws Exception {
		String suggestion = "";
		Client client = createClient();
		com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestTasks tasks0 = new com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestTasks()
				.setContent(text);
		com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestLabels labels0 = new com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestLabels()
				.setLabel("ad");
		com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestLabels labels1 = new com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestLabels()
				.setLabel("spam");
		com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestLabels labels2 = new com.aliyun.imageaudit20191230.models.ScanTextRequest.ScanTextRequestLabels()
				.setLabel("abuse");
		com.aliyun.imageaudit20191230.models.ScanTextRequest scanTextRequest = new com.aliyun.imageaudit20191230.models.ScanTextRequest()
				.setLabels(java.util.Arrays.asList(
						labels0,
						labels1,
						labels2
				)).setTasks(
						java.util.Arrays.asList(
								tasks0
						)
				);
		RuntimeOptions runtime = new RuntimeOptions();
		try {


			suggestion = client.scanTextWithOptions(scanTextRequest, runtime).getBody().getData().elements.get(0).results.get(0).suggestion;
			System.out.println(suggestion);
		} catch (TeaException error) {
			// 错误 message
			System.out.println(error.getMessage());
			// 诊断地址
			System.out.println(error.getData().get("Recommend"));
			com.aliyun.teautil.Common.assertAsString(error.message);
		} catch (Exception _error) {
			TeaException error = new TeaException(_error.getMessage(), _error);
			// 错误 message
			System.out.println(error.getMessage());
			// 诊断地址
			System.out.println(error.getData().get("Recommend"));
			com.aliyun.teautil.Common.assertAsString(error.message);
		}

		if(suggestion.equals("pass")){
			return true;
		}else{
			return false;
		}
	}

}
