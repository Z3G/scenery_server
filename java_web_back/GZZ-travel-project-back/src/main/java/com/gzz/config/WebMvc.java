package com.gzz.config;


import com.gzz.common.JacksonObjectMapper;
import com.gzz.common.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * 添加拦截器
 */
@Configuration
public class WebMvc extends WebMvcConfigurationSupport {

	@Resource
	StringRedisTemplate stringRedisTemplate;
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				//是否发送Cookie
				.allowCredentials(true)
				//放行哪些原始域
				.allowedOriginPatterns("*")
				.allowedMethods(new String[]{"GET", "POST", "PUT", "DELETE"})
				.allowedHeaders("*")
				.exposedHeaders("*");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		registry.addInterceptor(new LoginInterceptor(stringRedisTemplate)).addPathPatterns("/**").excludePathPatterns(
				"/api/user/login",
				"/api/strategy/list",
				"/api/scenery/list",
				"/api/scenery/detail",
				"/api/strategy/list",
				"/api/strategy/detail",
				"/api/activity/**",
				"/api/lost/list",
				"/api/admin/login",
				"/api/user/register",
				"/api/user/validate",
				"/api/home-page/**",
				"/api/food/**",
				"/api/route/khlist",
				"/api/route/detail",
				"/api/comment/list",
				"/api/user-like/getLike",
				"/api/user/isLogin"
		).order(1);
	}

	@Override
	protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
		mappingJackson2HttpMessageConverter.setObjectMapper(new JacksonObjectMapper());
		converters.add(0,mappingJackson2HttpMessageConverter);
		//super.extendMessageConverters(converters);
	}
}
