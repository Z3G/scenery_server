package com.gzz;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@SpringBootApplication
@Slf4j
@MapperScan("com.gzz.mapper")
@EnableScheduling
@EnableTransactionManagement
@EnableWebSocket
public class GzzProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(GzzProjectApplication.class,args);
	}

	@Bean
	public MessageConverter jacksonMessageConvertor(){
		return new Jackson2JsonMessageConverter();
	}
}
